# Desktop in Debug Mode

cargo run
MAKEPAD=lines sudo cargo +nightly run nimanyatta_v001

# Desktop in Release Mode

cargo run --release

# Desktop in small size

cargo run --profile=small

# Android

cargo makepad android run --release
sudo cargo makepad android run -p nimanyatta_v001 --release

You can also customize the package name and application label

```
cargo run -p cargo-makepad --release -- android --package-name=com.yourcompany.myapp --app-label="My Example App" run -p makepad-example-ironfish
```

# wasm

sudo cargo makepad wasm run -p nimanyatta_v001 --release

RUSTFLAGS="-C linker-plugin-lto -C embed-bitcode=yes -C
codegen-units=1 -C opt-level=z" sudo cargo +nightly build -p nimanyatta_v001 --target=wasm32-unknown-unknown --release
cargo +stable run -p wasm_strip --release -- target/wasm32-unknown-unknown/release/nimanyatta_v001.wasm
