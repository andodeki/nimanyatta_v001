use makepad_widgets::*;
// const CHOICE_MAX_OFFSET: f64 = 400.0;

live_design! {
    import makepad_draw::shader::std::*;
    import makepad_widgets::base::*;
    import makepad_widgets::theme_desktop_dark::*;
    import crate::shared::styles::*;
    import crate::shared::steps::StepNav;
    import crate::shared::custom_button::CustomButton;
    RoundSlider = <Slider> {
        // height: 36
        height: 300
        text: "CutOff1"
        draw_text: {text_style: <H2_TEXT_BOLD> {}, color: (COLOR_UP_5)}
        text_input: {
            cursor_margin_bottom: (SSPACING_1),
            cursor_margin_top: (SSPACING_1),
            select_pad_edges: (SSPACING_1),
            cursor_size: (SSPACING_1),
            empty_message: "0",
            numeric_only: true,
            draw_bg: {
                color: (COLOR_DOWN_0)
            },
        }
        draw_slider: {
            instance hover: float
            instance focus: float
            instance drag: float

            fn pixel(self) -> vec4 {
                let slider_height = 10;
                let offset = 150;
                let nub_size = mix(3, 4, self.hover);
                let nubbg_size = 18

                let sdf = Sdf2d::viewport(self.pos * self.rect_size)

                let slider_bg_color = mix(#38, #30, self.focus);
                let slider_color = mix(mix(#x086975, #x086975, self.hover), #x086975, self.focus);
                let nub_color = mix(mix(#8, #f, self.hover), mix(#c, #f, self.drag), self.focus);
                let nubbg_color = mix(#eee0, #8, self.drag);

                sdf.rect(0, self.rect_size.y - slider_height-offset, self.rect_size.x, slider_height)
                sdf.fill(slider_bg_color);

                sdf.rect(0, self.rect_size.y - slider_height-offset, self.slide_pos * (self.rect_size.x - nub_size) + nub_size, slider_height)
                sdf.fill(slider_color);

                let nubbg_x = self.slide_pos * (self.rect_size.x - nub_size) - nubbg_size * 0.5 + 0.5 * nub_size;
                sdf.rect(nubbg_x, self.rect_size.y - slider_height-offset, nubbg_size, slider_height)
                sdf.fill(nubbg_color);

                // the nub
                let nub_x = self.slide_pos * (self.rect_size.x - nub_size);
                sdf.circle(nub_x, self.rect_size.y - slider_height-(offset-5), slider_height);
                // sdf.rect(nub_x, self.rect_size.y - slider_height-100, nub_size, slider_height)
                sdf.fill_keep(mix(mix(#xF6FCFD, #xF6FCFD, self.hover), #xF6FCFD, self.pos.y)); // Nub background gradient
                sdf.stroke(
                    mix(
                        mix(#x00bbd3, #x00bbd3, self.hover),
                        #x00bbd3,
                        pow(self.pos.y, 1.5)
                    ),
                    1.
                ); // Nub outline gradient
                // sdf.fill(nub_color);

                // // let nub_x = self.slide_pos * (self.rect_size.x - nub_size - in_side * 2 - 9);
                // let cnub_x = self.slide_pos * (self.rect_size.x - nub_size - cin_side * 2 - 9);
                // let diameter = 7.0
                // // sdf.move_to(mix(in_side + 3.5, self.rect_size.x * 0.5, self.bipolar), top + in_top);
                // sdf.move_to(mix(cin_side + 3.5, self.rect_size.x * 0.5, self.bipolar), top + cin_top);

                // sdf.line_to(nub_x + cin_side + nub_size * 0.5, top + cin_top);
                // sdf.stroke_keep(mix((COLOR_UP_0), self.line_color, self.drag), 1.5)
                // sdf.stroke(
                //     mix(mix(self.line_color * 0.85, self.line_color, self.hover), #xFFFFFF80, self.drag),
                //     1
                // )

                // let nub_x = self.slide_pos * (self.rect_size.x - nub_size - in_side * 2 - 3) - 3;
                // sdf.circle(nub_x + in_side, top + 7.0, diameter);
                // sdf.fill_keep(mix(mix(#xF6FCFD, #xF6FCFD, self.hover), #xF6FCFD, self.pos.y)); // Nub background gradient
                // sdf.stroke(
                //     mix(
                //         mix(#x00bbd3, #x00bbd3, self.hover),
                //         #x00bbd3,
                //         pow(self.pos.y, 1.5)
                //     ),
                //     1.
                // ); // Nub outline gradient

                return sdf.result
            }
        }
    }
}
