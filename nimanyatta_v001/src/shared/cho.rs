use makepad_widgets::*;

live_design! {
    import makepad_draw::shader::std::*;
    import makepad_widgets::base::*;

    DrawStepsNav = {{DrawStepsNav}} {}
    StepsNavBase = {{StepsNav}} {}
    StepNav = <StepsNavBase> {

       width: Fit,
       height: Fit

       label_walk: {
           margin: {left: 20.0, top: 8, bottom: 8, right: 10}
           width: Fit,
           height: Fit,
       }

       label_align: {
           y: 0.0
       }

       draw_check: {
           uniform size: 7.0;
           fn pixel(self) -> vec4 {
               let sdf = Sdf2d::viewport(self.pos * self.rect_size)
               match self.check_type {
                   CheckType::Check => {
                       let left = 3;
                       let sz = self.size;
                       let c = vec2(left + sz, self.rect_size.y * 0.5);
                       sdf.box(left, c.y - sz, sz * 2.0, sz * 2.0, 3.0); // rounding = 3rd value
                       sdf.fill_keep(mix(mix(#x00000077, #x00000044, pow(self.pos.y, 1.)), mix(#x000000AA, #x00000066, pow(self.pos.y, 1.0)), self.hover))
                       sdf.stroke(#x888, 1.0) // outline
                       let szs = sz * 0.5;
                       let dx = 1.0;
                       sdf.move_to(left + 4.0, c.y);
                       sdf.line_to(c.x, c.y + szs);
                       sdf.line_to(c.x + szs, c.y - szs);
                       sdf.stroke(mix(#fff0, #f, self.selected), 1.25);
                   }
                   CheckType::Radio => {
                       let sz = self.size;
                       let left = sz + 1.;
                       let c = vec2(left + sz, self.rect_size.y * 0.5);
                       sdf.circle(left, c.y, sz);
                       sdf.fill(#2);
                       let isz = sz * 0.5;
                       sdf.circle(left, c.y, isz);
                       sdf.fill(mix(#fff0, #f, self.selected));
                   }
                   CheckType::Toggle => {
                       let sz = self.size;
                       let left = sz + 1.;
                       let c = vec2(left + sz, self.rect_size.y * 0.5);
                       sdf.box(left, c.y - sz, sz * 3.0, sz * 2.0, 0.5 * sz);
                       sdf.fill(#2);
                       let isz = sz * 0.5;
                       sdf.circle(left + sz + self.selected * sz, c.y, isz);
                       sdf.circle(left + sz + self.selected * sz, c.y, 0.5 * isz);
                       sdf.subtract();
                       sdf.circle(left + sz + self.selected * sz, c.y, isz);
                       sdf.blend(self.selected)
                       sdf.fill(#f);
                   }
                   CheckType::None => {
                       return #0000
                   }
                   CheckType::Bar => {
                       let sz = self.size;
                       let left = sz + 1.;
                       let up = sz + 3.5;
                       let c = self.rect_size * vec2(0.5, 0.5);
                       sdf.box(
                           left,
                           c.y - up,
                           12. * sz + 5,
                           1. * sz,
                           1.9
                       );

                       sdf.fill(#232323);
                       sdf.stroke(#000, 0.5 + 0.5 * self.dpi_dilate);

                       let isz = sz * 0.5;
                       let ileft = isz + 4.5;
                       let iup = sz + 3.5;
                       sdf.box(
                           ileft,
                           c.y - iup,
                           12. * sz + 5,
                           1. * sz,
                           1.9
                       );
                       sdf.fill(mix(#fff0, #016def, self.selected));
                   }
               }
               return sdf.result
           }
       }
       draw_text: {
           color: #9,
           instance focus: 0.0
           instance selected: 0.0
           instance hover: 0.0
           text_style: {
               font: {
                   //path: d"resources/IBMPlexSans-SemiBold.ttf"
               }
               font_size: 11.0
           }
           fn get_color(self) -> vec4 {
               return mix(
                   mix(
                       #fff6,
                       #fff6,
                       self.hover
                   ),
                   #fff6,
                   self.selected
               )
           }
       }

       draw_icon: {
           instance focus: 0.0
           instance hover: 0.0
           instance selected: 0.0
           fn get_color(self) -> vec4 {
               return mix(
                   mix(
                       #9,
                       #c,
                       self.hover
                   ),
                   #f,
                   self.selected
               )
           }
       }

       animator: {
           hover = {
               default: off
               off = {
                   from: {all: Forward {duration: 0.15}}
                   apply: {
                       draw_check: {hover: 0.0}
                       draw_text: {hover: 0.0}
                       draw_icon: {hover: 0.0}
                   }
               }
               on = {
                   from: {all: Snap}
                   apply: {
                       draw_check: {hover: 1.0}
                       draw_text: {hover: 1.0}
                       draw_icon: {hover: 1.0}
                   }
               }
           }
           focus = {
               default: off
               off = {
                   from: {all: Snap}
                   apply: {
                       draw_check: {focus: 0.0}
                       draw_text: {focus: 0.0}
                       draw_icon: {focus: 0.0}
                   }
               }
               on = {
                   from: {all: Snap}
                   apply: {
                       draw_check: {focus: 1.0}
                       draw_text: {focus: 1.0}
                       draw_icon: {focus: 1.0}
                   }
               }
           }
           selected = {
               default: off
               off = {
                   from: {all: Forward {duration: 0.1}}
                   apply: {
                       draw_check: {selected: 0.0},
                       draw_text: {selected: 0.0},
                       draw_icon: {selected: 0.0},
                   }
               }
               on = {
                   from: {all: Forward {duration: 0.0}}
                   apply: {
                       draw_check: {selected: 1.0}
                       draw_text: {selected: 1.0}
                       draw_icon: {selected: 1.0},
                   }
               }
           }
       }
   }
   Cho = <StepNav>{
       width: 100, height: 20,
       margin: 0, padding: 0
       flow: Right,
       align: {x: 0.5, y: 0.5},
       draw_check: {
           check_type: Bar,
       }
   }
   // CheckContainer = <View> {
   //     width: Fit,
   //     height: Fit,
   //     checkbox = <CheckBox> {
   //         width: 400
   //         height: 266
   //         text: "Check me out!"
   //     }
   // }
   ModelFilesTags = {{ModelFilesTags}} {
        width: Fit,
        height: Fit,
        flow: Right,
        spacing: 5,

        template: <Cho> {}
    }
}

// #[derive(Live, Widget)]
// pub struct Choices {
//     #[redraw]
//     #[live]
//     steps: ScrollBars,
// }

#[derive(Live, LiveHook, Widget)]
pub struct ModelFilesTags {
    #[redraw]
    #[rust]
    area: Area,

    #[walk]
    walk: Walk,

    #[layout]
    layout: Layout,

    #[live]
    template: Option<LivePtr>,

    #[rust]
    items: ComponentMap<LiveId, WidgetRef>,
}
// impl LiveHook for ModelFilesTags {
//     fn after_apply(&mut self, _cx: &mut Cx, _apply: &mut Apply, _index: usize, _nodes: &[LiveNode]) {
//         self.set_tags
//     }

// }

impl Widget for ModelFilesTags {
    fn handle_event(&mut self, cx: &mut Cx, event: &Event, scope: &mut Scope) {
        for (_id, item) in self.items.iter_mut() {
            item.handle_event(cx, event, scope);
        }
    }

    fn draw_walk(&mut self, cx: &mut Cx2d, scope: &mut Scope, walk: Walk) -> DrawStep {
        cx.begin_turtle(walk, self.layout);
        for (_id, item) in self.items.iter_mut() {
            let _ = item.draw_walk(cx, scope, walk);
        }
        cx.end_turtle_with_area(&mut self.area);
        DrawStep::done()
    }
}

// impl LiveHook for ModelFilesTags {
//     fn before_live_design(cx: &mut Cx) {
//         register_widget!(cx, ModelFilesTags);
//     }
// }
impl ModelFilesTagsRef {
    pub fn set_tags(&self, cx: &mut Cx, tags: Vec<String>) {
        let Some(mut tags_widget) = self.borrow_mut() else {
            return;
        };
        tags_widget.items.clear();
        for (i, tag) in tags.iter().enumerate() {
            let item_id = LiveId(i as u64).into();
            let item_widget = WidgetRef::new_from_ptr(cx, tags_widget.template);
            item_widget.apply_over(cx, live! {label = { text: (tag) }});
            tags_widget.items.insert(item_id, item_widget);
        }
    }
}
#[derive(Live, LiveHook, LiveRegister)]
#[repr(C)]
pub struct DrawStepsNav {
    #[deref]
    draw_super: DrawQuad,
    #[live]
    check_type: CheckType,
    #[live]
    hover: f32,
    #[live]
    focus: f32,
    #[live]
    selected: f32,
}

#[derive(Live, LiveHook, Widget)]
pub struct StepsNav {
    #[walk]
    walk: Walk,
    #[layout]
    layout: Layout,
    #[animator]
    animator: Animator,

    #[live]
    icon_walk: Walk,
    #[live]
    label_walk: Walk, //
    #[live]
    #[redraw]
    draw_name: DrawText, //
    #[live]
    label_align: Align, //

    #[redraw]
    #[live]
    draw_check: DrawStepsNav, //
    #[live]
    draw_text: DrawText,
    #[live]
    draw_icon: DrawIcon,

    #[live]
    text: RcStringMut, //

    #[live]
    bind: String, //
}
#[derive(Live, LiveHook, LiveRegister)]
#[live_ignore]
#[repr(u32)]
pub enum CheckType {
    #[pick]
    Check = shader_enum(1),
    Radio = shader_enum(2),
    Toggle = shader_enum(3),
    None = shader_enum(4),
    Bar = shader_enum(5), //RadioType::Bar
}

#[derive(Default, Clone, Debug)]
pub enum StepsNavAction {
    Change(bool),
    WasSweeped,
    WasSelected,
    #[default]
    None,
}
impl StepsNav {
    pub fn draw_walk(&mut self, cx: &mut Cx2d, walk: Walk) {
        self.draw_check.begin(cx, walk, self.layout);
        // self.draw_icon.svg_file = icon;

        self.draw_text
            .draw_walk(cx, self.label_walk, self.label_align, self.text.as_ref());
        self.draw_icon.draw_walk(cx, self.icon_walk);
        self.draw_check.end(cx);
    }
    pub fn handle_event_with(
        &mut self,
        cx: &mut Cx,
        event: &Event,
        sweep_area: Area,
        dispatch_action: &mut dyn FnMut(&mut Cx, StepsNavAction),
    ) {
        if self.animator_handle_event(cx, event).must_redraw() {
            // self.draw_bg.area().redraw(cx);
            self.draw_check.area().redraw(cx);
        }

        match event.hits_with_options(
            cx,
            // self.draw_bg.area(),
            self.draw_check.area(),
            HitOptions::new().with_sweep_area(sweep_area),
        ) {
            Hit::FingerHoverIn(_) => {
                self.animator_play(cx, id!(hover.on));
            }
            Hit::FingerHoverOut(_) => {
                self.animator_play(cx, id!(hover.off));
            }
            Hit::FingerDown(_) => {
                dispatch_action(cx, StepsNavAction::WasSweeped);
                self.animator_play(cx, id!(hover.on));
                self.animator_play(cx, id!(select.on));
            }
            Hit::FingerUp(se) => {
                if !se.is_sweep {
                    dispatch_action(cx, StepsNavAction::WasSelected);
                } else {
                    self.animator_play(cx, id!(hover.off));
                    self.animator_play(cx, id!(select.off));
                }
            }
            _ => {}
        }
    }
}

impl Widget for StepsNav {
    fn widget_to_data(
        &self,
        _cx: &mut Cx,
        actions: &Actions,
        nodes: &mut LiveNodeVec,
        path: &[LiveId],
    ) -> bool {
        match actions.find_widget_action_cast(self.widget_uid()) {
            StepsNavAction::Change(v) => {
                nodes.write_field_value(path, LiveValue::Bool(v));
                true
            }
            _ => false,
        }
    }

    fn data_to_widget(&mut self, cx: &mut Cx, nodes: &[LiveNode], path: &[LiveId]) {
        if let Some(value) = nodes.read_field_value(path) {
            if let Some(value) = value.as_bool() {
                self.animator_toggle(cx, value, Animate::Yes, id!(selected.on), id!(selected.off));
            }
        }
    }

    fn handle_event(&mut self, cx: &mut Cx, event: &Event, scope: &mut Scope) {
        let uid = self.widget_uid();
        self.animator_handle_event(cx, event);

        match event.hits(cx, self.draw_check.area()) {
            Hit::FingerHoverIn(_) => {
                cx.set_cursor(MouseCursor::Hand);
                self.animator_play(cx, id!(hover.on));
            }
            Hit::FingerHoverOut(_) => {
                self.animator_play(cx, id!(hover.off));
            }
            Hit::FingerDown(_fe) => {
                if self.animator_in_state(cx, id!(selected.on)) {
                    self.animator_play(cx, id!(selected.off));
                    cx.widget_action(uid, &scope.path, StepsNavAction::Change(false));
                } else {
                    self.animator_play(cx, id!(selected.on));
                    cx.widget_action(uid, &scope.path, StepsNavAction::Change(true));
                }
            }
            Hit::FingerUp(_fe) => {}
            Hit::FingerMove(_fe) => {}
            _ => (),
        }
    }

    fn draw_walk(&mut self, cx: &mut Cx2d, _scope: &mut Scope, walk: Walk) -> DrawStep {
        self.draw_walk(cx, walk);
        DrawStep::done()
    }

    fn text(&self) -> String {
        self.text.as_ref().to_string()
    }

    fn set_text(&mut self, v: &str) {
        self.text.as_mut_empty().push_str(v);
    }
}
