use std::{cell::RefCell, rc::Rc};

use makepad_widgets::*;

live_design! {
    import makepad_draw::shader::std::*;
    import makepad_widgets::base::*;

    DrawStepsNav = {{DrawStepsNav}} {}
    StepsNavBase = {{StepsNav}} {}
    StepNav = <StepsNavBase> {

       width: Fit,
       height: Fit

       label_walk: {
           margin: {left: 20.0, top: 8, bottom: 8, right: 10}
           width: Fit,
           height: Fit,
       }

       label_align: {
           y: 0.0
       }

       draw_check: {
           uniform size: 7.0;
           fn pixel(self) -> vec4 {
               let sdf = Sdf2d::viewport(self.pos * self.rect_size)
               match self.check_type {
                   CheckType::Check => {
                       let left = 3;
                       let sz = self.size;
                       let c = vec2(left + sz, self.rect_size.y * 0.5);
                       sdf.box(left, c.y - sz, sz * 2.0, sz * 2.0, 3.0); // rounding = 3rd value
                       sdf.fill_keep(mix(mix(#x00000077, #x00000044, pow(self.pos.y, 1.)), mix(#x000000AA, #x00000066, pow(self.pos.y, 1.0)), self.hover))
                       sdf.stroke(#x888, 1.0) // outline
                       let szs = sz * 0.5;
                       let dx = 1.0;
                       sdf.move_to(left + 4.0, c.y);
                       sdf.line_to(c.x, c.y + szs);
                       sdf.line_to(c.x + szs, c.y - szs);
                       sdf.stroke(mix(#fff0, #f, self.selected), 1.25);
                   }
                   CheckType::Radio => {
                       let sz = self.size;
                       let left = sz + 1.;
                       let c = vec2(left + sz, self.rect_size.y * 0.5);
                       sdf.circle(left, c.y, sz);
                       sdf.fill(#2);
                       let isz = sz * 0.5;
                       sdf.circle(left, c.y, isz);
                       sdf.fill(mix(#fff0, #f, self.selected));
                   }
                   CheckType::Toggle => {
                       let sz = self.size;
                       let left = sz + 1.;
                       let c = vec2(left + sz, self.rect_size.y * 0.5);
                       sdf.box(left, c.y - sz, sz * 3.0, sz * 2.0, 0.5 * sz);
                       sdf.fill(#2);
                       let isz = sz * 0.5;
                       sdf.circle(left + sz + self.selected * sz, c.y, isz);
                       sdf.circle(left + sz + self.selected * sz, c.y, 0.5 * isz);
                       sdf.subtract();
                       sdf.circle(left + sz + self.selected * sz, c.y, isz);
                       sdf.blend(self.selected)
                       sdf.fill(#f);
                   }
                   CheckType::None => {
                       return #0000
                   }
                   CheckType::Bar => {
                       let sz = self.size;
                       let left = sz + 1.;
                       let up = sz + 3.5;
                       let c = self.rect_size * vec2(0.5, 0.5);
                       sdf.box(
                           left,
                           c.y - up,
                           12. * sz + 5,
                           1. * sz,
                           1.9
                       );

                       sdf.fill(#232323);
                       sdf.stroke(#000, 0.5 + 0.5 * self.dpi_dilate);

                       let isz = sz * 0.5;
                       let ileft = isz + 4.5;
                       let iup = sz + 3.5;
                       sdf.box(
                           ileft,
                           c.y - iup,
                           12. * sz + 5,
                           1. * sz,
                           1.9
                       );
                       sdf.fill(mix(#fff0, #016def, self.selected));
                   }
               }
               return sdf.result
           }
       }
       draw_text: {
           color: #9,
           instance focus: 0.0
           instance selected: 0.0
           instance hover: 0.0
           text_style: {
               font: {
                   //path: d"resources/IBMPlexSans-SemiBold.ttf"
               }
               font_size: 11.0
           }
           fn get_color(self) -> vec4 {
               return mix(
                   mix(
                       #fff6,
                       #fff6,
                       self.hover
                   ),
                   #fff6,
                   self.selected
               )
           }
       }

       draw_icon: {
           instance focus: 0.0
           instance hover: 0.0
           instance selected: 0.0
           fn get_color(self) -> vec4 {
               return mix(
                   mix(
                       #9,
                       #c,
                       self.hover
                   ),
                   #f,
                   self.selected
               )
           }
       }

       animator: {
           hover = {
               default: off
               off = {
                   from: {all: Forward {duration: 0.15}}
                   apply: {
                       draw_check: {hover: 0.0}
                       draw_text: {hover: 0.0}
                       draw_icon: {hover: 0.0}
                   }
               }
               on = {
                   from: {all: Snap}
                   apply: {
                       draw_check: {hover: 1.0}
                       draw_text: {hover: 1.0}
                       draw_icon: {hover: 1.0}
                   }
               }
           }
           focus = {
               default: off
               off = {
                   from: {all: Snap}
                   apply: {
                       draw_check: {focus: 0.0}
                       draw_text: {focus: 0.0}
                       draw_icon: {focus: 0.0}
                   }
               }
               on = {
                   from: {all: Snap}
                   apply: {
                       draw_check: {focus: 1.0}
                       draw_text: {focus: 1.0}
                       draw_icon: {focus: 1.0}
                   }
               }
           }
           selected = {
               default: off
               off = {
                   from: {all: Forward {duration: 0.1}}
                   apply: {
                       draw_check: {selected: 0.0},
                       draw_text: {selected: 0.0},
                       draw_icon: {selected: 0.0},
                   }
               }
               on = {
                   from: {all: Forward {duration: 0.0}}
                   apply: {
                       draw_check: {selected: 1.0}
                       draw_text: {selected: 1.0}
                       draw_icon: {selected: 1.0},
                   }
               }
           }
       }
   }
   Choices = {{Choices}} {
       <View> {
           flow: Down,
           align: {x: 0.5, y: 0.3}
           debug: A
           choice_item: <StepNav> {
               width: 100, height: 20,
               margin: 0, padding: 0
               flow: Right,
               align: {x: 0.5, y: 0.5},
               // show_bg: true
               draw_check: {
                   check_type: Bar,
               }
           }
           width: 100, height: 100,
           // flow: Down,
       }


   }

   ChoicesComponent = {{ChoicesComponent}} {
       choices = <Choices>{}
   }
   // Choices = <StepNav> {
   //     width: 100, height: 20,
   //     margin: 0, padding: 0
   //     flow: Right,
   //     align: {x: 0.5, y: 0.5},
   //     // show_bg: true
   //     draw_check: {
   //         check_type: Bar,
   //     }
   // }
}
#[derive(Live, Widget)]
pub struct ChoicesComponent {
    #[animator]
    animator: Animator,

    #[walk]
    walk: Walk,
    #[layout]
    layout: Layout,

    #[live]
    #[redraw]
    draw_bg: DrawQuad,
    #[live]
    draw_icon: DrawIcon,
    #[live]
    icon_walk: Walk,

    #[live]
    bind: String,
    #[live]
    bind_enum: String,

    #[live]
    choices: Option<LivePtr>,

    #[live]
    labels: Vec<String>,
    #[live]
    values: Vec<LiveValue>,
    #[live]
    icons: Vec<LiveDependency>,

    #[live]
    popup_shift: DVec2,

    #[rust]
    is_open: bool,

    #[live]
    selected_item: usize,
}

impl LiveHook for ChoicesComponent {
    fn after_apply_from(&mut self, cx: &mut Cx, apply: &mut Apply) {
        if self.choices.is_none() || !apply.from.is_from_doc() {
            return;
        }
        let global = cx.global::<ChoicesGlobal>().clone();
        let mut map = global.map.borrow_mut();

        // when live styling clean up old style references
        map.retain(|k, _| cx.live_registry.borrow().generation_valid(*k));

        let list_box = self.choices.unwrap();
        map.get_or_insert(cx, list_box, |cx| Choices::new_from_ptr(cx, Some(list_box)));
    }
}
#[derive(Clone, DefaultNone, Debug)]
pub enum ChoicesComponentAction {
    Select(usize, LiveValue),
    None,
}
impl Widget for ChoicesComponent {
    fn widget_to_data(
        &self,
        _cx: &mut Cx,
        actions: &Actions,
        nodes: &mut LiveNodeVec,
        path: &[LiveId],
    ) -> bool {
        match actions.find_widget_action(self.widget_uid()).cast() {
            ChoicesComponentAction::Select(_, value) => {
                nodes.write_field_value(path, value.clone());
                true
            }
            _ => false,
        }
    }

    fn data_to_widget(&mut self, cx: &mut Cx, nodes: &[LiveNode], path: &[LiveId]) {
        if let Some(value) = nodes.read_field_value(path) {
            if let Some(index) = self.values.iter().position(|v| v == value) {
                if self.selected_item != index {
                    self.selected_item = index;
                    self.redraw(cx);
                }
            } else {
                // error!("Value not in values list {:?}", value);
            }
        }
    }

    fn handle_event(&mut self, cx: &mut Cx, event: &Event, scope: &mut Scope) {
        self.animator_handle_event(cx, event);
        let uid = self.widget_uid();

        if self.choices.is_some() {
            let global = cx.global::<ChoicesGlobal>().clone();
            let mut map = global.map.borrow_mut();
            let menu = map.get_mut(&self.choices.unwrap()).unwrap();
            let mut close = false;
            menu.handle_event_with(cx, event, self.draw_bg.area(), &mut |cx, action| {
                if let ChoicesAction::WasSelected(node_id) = action {
                    self.selected_item = node_id.0 .0 as usize;
                    cx.widget_action(
                        uid,
                        &scope.path,
                        ChoicesComponentAction::Select(
                            self.selected_item,
                            self.values
                                .get(self.selected_item)
                                .cloned()
                                .unwrap_or(LiveValue::None),
                        ),
                    );
                    self.draw_bg.redraw(cx);
                    close = true;
                }
            });
            // if close {
            //     self.set_closed(cx);
            // }

            // check if we touch outside of the popup menu
            if let Event::MouseDown(e) = event {
                if !menu.menu_contains_pos(cx, e.abs) {
                    // self.set_closed(cx);
                    self.animator_play(cx, id!(hover.off));
                }
            }
        }
        // TODO: close on clicking outside of the popup menu
        match event.hits_with_sweep_area(cx, self.draw_bg.area(), self.draw_bg.area()) {
            Hit::KeyFocusLost(_) => {
                // self.set_closed(cx);
                self.animator_play(cx, id!(hover.off));
                self.draw_bg.redraw(cx);
            }
            Hit::KeyDown(ke) => match ke.key_code {
                KeyCode::ArrowUp => {
                    if self.selected_item > 0 {
                        self.selected_item -= 1;
                        cx.widget_action(
                            uid,
                            &scope.path,
                            ChoicesComponentAction::Select(
                                self.selected_item,
                                self.values
                                    .get(self.selected_item)
                                    .cloned()
                                    .unwrap_or(LiveValue::None),
                            ),
                        );
                        // self.set_closed(cx);
                        self.draw_bg.redraw(cx);
                    }
                }
                KeyCode::ArrowDown => {
                    if !self.values.is_empty() && self.selected_item < self.values.len() - 1 {
                        self.selected_item += 1;
                        cx.widget_action(
                            uid,
                            &scope.path,
                            ChoicesComponentAction::Select(
                                self.selected_item,
                                self.values
                                    .get(self.selected_item)
                                    .cloned()
                                    .unwrap_or(LiveValue::None),
                            ),
                        );
                        // self.set_closed(cx);
                        self.draw_bg.redraw(cx);
                    }
                }
                _ => (),
            },
            Hit::FingerDown(_fe) => {
                cx.set_key_focus(self.draw_bg.area());
                // self.toggle_open(cx);
                self.animator_play(cx, id!(hover.pressed));
            }
            Hit::FingerHoverIn(_) => {
                cx.set_cursor(MouseCursor::Hand);
                self.animator_play(cx, id!(hover.on));
            }
            Hit::FingerHoverOut(_) => {
                self.animator_play(cx, id!(hover.off));
            }
            Hit::FingerUp(fe) => {
                if fe.is_over {
                    if fe.device.has_hovers() {
                        self.animator_play(cx, id!(hover.on));
                    }
                } else {
                    self.animator_play(cx, id!(hover.off));
                }
            }
            _ => (),
        };
    }

    fn draw_walk(&mut self, cx: &mut Cx2d, _scope: &mut Scope, walk: Walk) -> DrawStep {
        // cx.clear_sweep_lock(self.draw_bg.area());

        self.draw_bg.begin(cx, walk, self.layout);
        //let start_pos = cx.turtle().rect().pos;
        self.draw_icon.draw_walk(cx, self.icon_walk);
        self.draw_bg.end(cx);

        // cx.add_nav_stop(self.draw_bg.area(), NavRole::DropDown, Margin::default());

        if self.choices.is_some() {
            // cx.set_sweep_lock(self.draw_bg.area());
            let global = cx.global::<ChoicesGlobal>().clone();
            let mut map = global.map.borrow_mut();
            let popup_menu = map.get_mut(&self.choices.unwrap()).unwrap();

            popup_menu.begin(cx);

            for (i, item) in self.labels.iter().enumerate() {
                let node_id = LiveId(i as u64).into();
                popup_menu.draw_item(cx, node_id, item, walk);
            }

            popup_menu.end(cx, self.draw_bg.area());
        }

        DrawStep::done()
    }
}

impl ChoicesComponentRef {
    pub fn item_clicked(&mut self, item_id: &[LiveId], actions: &Actions) -> bool {
        if let Some(item) = actions.find_widget_action(self.widget_uid()) {
            if let ChoicesComponentAction::Select(_id, value) = item.cast() {
                return LiveValue::Bool(true) == value.enum_eq(item_id);
            }
        }
        return false;
    }
}
#[derive(Live, LiveHook, LiveRegister)]
#[repr(C)]
pub struct DrawStepsNav {
    #[deref]
    draw_super: DrawQuad,
    #[live]
    check_type: CheckType,
    #[live]
    hover: f32,
    #[live]
    focus: f32,
    #[live]
    selected: f32,
}

#[derive(Live, LiveHook, Widget)]
pub struct StepsNav {
    #[walk]
    walk: Walk,
    #[layout]
    layout: Layout,
    #[animator]
    animator: Animator,

    #[live]
    icon_walk: Walk,
    #[live]
    label_walk: Walk, //
    #[live]
    #[redraw]
    draw_name: DrawText, //
    #[live]
    label_align: Align, //

    #[redraw]
    #[live]
    draw_check: DrawStepsNav, //
    #[live]
    draw_text: DrawText,
    #[live]
    draw_icon: DrawIcon,

    #[live]
    text: RcStringMut, //

    #[live]
    bind: String, //
}
#[derive(Clone, Debug, Default, Eq, Hash, Copy, PartialEq, FromLiveId)]
pub struct StepsNavId(pub LiveId);

#[derive(Live, LiveHook, LiveRegister)]
#[live_ignore]
#[repr(u32)]
pub enum CheckType {
    #[pick]
    Check = shader_enum(1),
    Radio = shader_enum(2),
    Toggle = shader_enum(3),
    None = shader_enum(4),
    Bar = shader_enum(5), //RadioType::Bar
}

// #[derive(Clone, Debug, DefaultNone)]
// pub enum StepsNavAction {
//     Change(bool),
//     None,
// }
#[derive(Default, Clone, Debug)]
pub enum StepsNavAction {
    Change(bool),
    WasSweeped,
    WasSelected,
    #[default]
    None,
}
#[derive(Clone, DefaultNone, Debug)]
pub enum ChoicesAction {
    WasSweeped(StepsNavId),
    WasSelected(StepsNavId),
    None,
}

impl StepsNav {
    pub fn draw_walk(&mut self, cx: &mut Cx2d, walk: Walk) {
        self.draw_check.begin(cx, walk, self.layout);
        // self.draw_icon.svg_file = icon;

        self.draw_text
            .draw_walk(cx, self.label_walk, self.label_align, self.text.as_ref());
        self.draw_icon.draw_walk(cx, self.icon_walk);
        self.draw_check.end(cx);
    }
    pub fn handle_event_with(
        &mut self,
        cx: &mut Cx,
        event: &Event,
        sweep_area: Area,
        dispatch_action: &mut dyn FnMut(&mut Cx, StepsNavAction),
    ) {
        if self.animator_handle_event(cx, event).must_redraw() {
            // self.draw_bg.area().redraw(cx);
            self.draw_check.area().redraw(cx);
        }

        match event.hits_with_options(
            cx,
            // self.draw_bg.area(),
            self.draw_check.area(),
            HitOptions::new().with_sweep_area(sweep_area),
        ) {
            Hit::FingerHoverIn(_) => {
                self.animator_play(cx, id!(hover.on));
            }
            Hit::FingerHoverOut(_) => {
                self.animator_play(cx, id!(hover.off));
            }
            Hit::FingerDown(_) => {
                dispatch_action(cx, StepsNavAction::WasSweeped);
                self.animator_play(cx, id!(hover.on));
                self.animator_play(cx, id!(select.on));
            }
            Hit::FingerUp(se) => {
                if !se.is_sweep {
                    dispatch_action(cx, StepsNavAction::WasSelected);
                } else {
                    self.animator_play(cx, id!(hover.off));
                    self.animator_play(cx, id!(select.off));
                }
            }
            _ => {}
        }
    }
}

impl Widget for StepsNav {
    fn widget_to_data(
        &self,
        _cx: &mut Cx,
        actions: &Actions,
        nodes: &mut LiveNodeVec,
        path: &[LiveId],
    ) -> bool {
        match actions.find_widget_action_cast(self.widget_uid()) {
            StepsNavAction::Change(v) => {
                nodes.write_field_value(path, LiveValue::Bool(v));
                true
            }
            _ => false,
        }
    }

    fn data_to_widget(&mut self, cx: &mut Cx, nodes: &[LiveNode], path: &[LiveId]) {
        if let Some(value) = nodes.read_field_value(path) {
            if let Some(value) = value.as_bool() {
                self.animator_toggle(cx, value, Animate::Yes, id!(selected.on), id!(selected.off));
            }
        }
    }

    fn handle_event(&mut self, cx: &mut Cx, event: &Event, scope: &mut Scope) {
        let uid = self.widget_uid();
        self.animator_handle_event(cx, event);

        match event.hits(cx, self.draw_check.area()) {
            Hit::FingerHoverIn(_) => {
                cx.set_cursor(MouseCursor::Hand);
                self.animator_play(cx, id!(hover.on));
            }
            Hit::FingerHoverOut(_) => {
                self.animator_play(cx, id!(hover.off));
            }
            Hit::FingerDown(_fe) => {
                if self.animator_in_state(cx, id!(selected.on)) {
                    self.animator_play(cx, id!(selected.off));
                    cx.widget_action(uid, &scope.path, StepsNavAction::Change(false));
                } else {
                    self.animator_play(cx, id!(selected.on));
                    cx.widget_action(uid, &scope.path, StepsNavAction::Change(true));
                }
            }
            Hit::FingerUp(_fe) => {}
            Hit::FingerMove(_fe) => {}
            _ => (),
        }
    }

    fn draw_walk(&mut self, cx: &mut Cx2d, _scope: &mut Scope, walk: Walk) -> DrawStep {
        self.draw_walk(cx, walk);
        DrawStep::done()
    }

    fn text(&self) -> String {
        self.text.as_ref().to_string()
    }

    fn set_text(&mut self, v: &str) {
        self.text.as_mut_empty().push_str(v);
    }
}

#[derive(Default, Clone)]
struct ChoicesGlobal {
    map: Rc<RefCell<ComponentMap<LivePtr, Choices>>>,
}

#[derive(Live, LiveRegister, WidgetWrap)]
pub struct Choices {
    #[live]
    draw_list: DrawList2d,

    #[live]
    choice_item: Option<LivePtr>,

    #[live]
    #[redraw]
    draw_bg: DrawQuad,
    #[live]
    #[redraw]
    draw_icon: DrawIcon,
    #[live]
    icon_walk: Walk,

    #[layout]
    layout: Layout,
    #[walk]
    walk: Walk,

    #[live]
    labels: Vec<String>,
    #[live]
    values: Vec<LiveValue>,

    #[live]
    items: Vec<String>,
    #[rust]
    choice_items: ComponentMap<StepsNavId, StepsNav>,
    #[rust]
    init_select_item: Option<StepsNavId>,
    #[rust]
    first_tap: bool,
    #[rust]
    count: usize,
}

impl LiveHook for Choices {
    fn after_apply(&mut self, cx: &mut Cx, apply: &mut Apply, index: usize, nodes: &[LiveNode]) {
        if let Some(index) = nodes.child_by_name(index, live_id!(list_node).as_field()) {
            for (_, node) in self.choice_items.iter_mut() {
                node.apply(cx, apply, index, nodes);
            }
        }
        self.draw_list.redraw(cx);
    }
}

impl Widget for Choices {
    fn handle_event(&mut self, _cx: &mut Cx, _event: &Event, _scope: &mut Scope) {}

    fn draw_walk(&mut self, cx: &mut Cx2d, _scope: &mut Scope, walk: Walk) -> DrawStep {
        self.draw_bg.begin(cx, walk, self.layout);
        self.draw_icon.draw_walk(cx, self.icon_walk);
        self.draw_bg.end(cx);

        DrawStep::done()
    }
}

impl Choices {
    pub fn menu_contains_pos(&self, cx: &mut Cx, pos: DVec2) -> bool {
        self.draw_bg.area().clipped_rect(cx).contains(pos)
    }

    pub fn begin(&mut self, cx: &mut Cx2d) {
        self.draw_list.begin_overlay_reuse(cx);

        cx.begin_pass_sized_turtle(Layout::flow_down());

        self.draw_bg.begin(cx, self.walk, self.layout);
        self.count = 0;
    }

    pub fn end(&mut self, cx: &mut Cx2d, shift_area: Area) {
        self.draw_bg.end(cx);
        let area = self.draw_bg.area().rect(cx);
        let shift = DVec2 {
            x: -area.size.x + (shift_area.rect(cx).size.x * 0.7),
            y: 30.,
        };

        cx.end_pass_sized_turtle_with_shift(shift_area, shift);
        self.draw_list.end(cx);

        self.choice_items.retain_visible();
        if let Some(init_select_item) = self.init_select_item.take() {
            self.select_item_state(cx, init_select_item);
        }
    }

    pub fn draw_item(
        &mut self,
        cx: &mut Cx2d,
        item_id: StepsNavId,
        label: &str,
        // icon: LiveDependency,
        walk: Walk,
    ) {
        self.count += 1;

        let menu_item = self.choice_item;
        let menu_item = self
            .choice_items
            .get_or_insert(cx, item_id, |cx| StepsNav::new_from_ptr(cx, menu_item));
        // menu_item.draw_walk(cx, label, icon);
        menu_item.draw_walk(cx, walk);
    }

    pub fn init_select_item(&mut self, which_id: StepsNavId) {
        self.init_select_item = Some(which_id);
        self.first_tap = true;
    }

    fn select_item_state(&mut self, cx: &mut Cx, which_id: StepsNavId) {
        for (id, item) in &mut *self.choice_items {
            if *id == which_id {
                item.animator_cut(cx, id!(select.on));
                item.animator_cut(cx, id!(hover.on));
            } else {
                item.animator_cut(cx, id!(select.off));
                item.animator_cut(cx, id!(hover.off));
            }
        }
    }

    pub fn handle_event_with(
        &mut self,
        cx: &mut Cx,
        event: &Event,
        sweep_area: Area,
        dispatch_action: &mut dyn FnMut(&mut Cx, ChoicesAction),
    ) {
        let mut actions = Vec::new();
        for (item_id, node) in self.choice_items.iter_mut() {
            node.handle_event_with(cx, event, sweep_area, &mut |_, e| {
                actions.push((*item_id, e))
            });
        }

        for (node_id, action) in actions {
            match action {
                StepsNavAction::WasSweeped => {
                    self.select_item_state(cx, node_id);
                    dispatch_action(cx, ChoicesAction::WasSweeped(node_id));
                }
                StepsNavAction::WasSelected => {
                    self.select_item_state(cx, node_id);
                    dispatch_action(cx, ChoicesAction::WasSelected(node_id));
                }
                _ => (),
            }
        }
    }
}
