use makepad_widgets::{makepad_derive_widget::*, makepad_draw::*, widget::*};

live_design! {
    import makepad_draw::shader::std::*;
    import makepad_widgets::base::*;
    import makepad_widgets::theme_desktop_dark::*;

    import crate::shared::styles::*;

    CustomButton = <Button> {
            width: Fill, height: 34.
            text: "Welcome"
            draw_text: {
                text_style: <REGULAR_TEXT>{font_size: 12.},
                fn get_color(self) -> vec4 {
                    // return #016def
                    return mix(mix(#016def, #f0, self.hover), #0157c0, self.pressed)
                }
            }
            draw_bg: {
                border_radius: 8.
                fn pixel(self) -> vec4 {
                    // let border_color = #016def; //#0157c0
                    let border_color = #0157c0;
                    let border_width = 0.5;
                    let sdf = Sdf2d::viewport(self.pos * self.rect_size);
                    let body = mix(mix(#f, #0157c0, self.hover), #d1, self.pressed);

                    sdf.box(
                        1.,
                        1.,
                        self.rect_size.x - 2.0,
                        self.rect_size.y - 2.0,
                        self.border_radius
                    )
                    sdf.fill_keep(body)

                    sdf.stroke(
                        border_color,
                        border_width
                    )
                    return sdf.result
                }
            }
    }

    // CustomButtonC = {{CustomButton}} {
    //     draw_cbutt: {
    //         <CustomButton>{}
    //     }
    // }

}

// #[derive(Live, LiveHook, LiveRegister)]
// #[repr(C)]
// pub struct DrawCButton {
//     #[deref]
//     draw_super: DrawQuad,
//     #[live]
//     button_type: CButtonType,
//     #[live]
//     hover: f32,
//     #[live]
//     focus: f32,
//     #[live]
//     selected: f32,
// }

// #[derive(Live, LiveHook)]
// #[live_ignore]
// #[repr(u32)]
// pub enum CButtonType {
//     #[pick]
//     Primary = shader_enum(1),
//     Secondary = shader_enum(2),
// }

// #[derive(Live, LiveHook, Widget)]
// pub struct CustomButton {
//     #[redraw]
//     #[live]
//     draw_cbtn: DrawCButton,
// }
// #[derive(Live, LiveHook, Widget)]
// pub struct CustomButton {
//     #[walk]
//     walk: Walk,
//     #[layout]
//     layout: Layout,
//     #[animator]
//     animator: Animator,

//     #[live]
//     icon_walk: Walk,
//     #[live]
//     label_walk: Walk,
//     #[live]
//     label_align: Align,

//     // #[redraw]
//     // #[live]
//     // draw_check: DrawCheckBox,
//     #[live]
//     #[redraw]
//     draw_cbutt: DrawCButton,
//     #[live]
//     draw_text: DrawText,
//     #[live]
//     draw_icon: DrawIcon,

//     #[live]
//     text: RcStringMut,

//     #[live]
//     bind: String,
// }

// #[derive(Live, LiveHook, LiveRegister)]
// #[repr(C)]
// pub struct DrawCButton {
//     #[deref]
//     draw_super: DrawQuad,
//     #[live]
//     check_type: CButtonType,
//     #[live]
//     hover: f32,
//     #[live]
//     focus: f32,
//     #[live]
//     selected: f32,
// }

// #[derive(Live, LiveHook, LiveRegister)]
// #[live_ignore]
// #[repr(u32)]
// pub enum CButtonType {
//     #[pick]
//     Clickable = shader_enum(1),
//     UnClickable = shader_enum(2),
// }

// #[derive(Clone, Debug, DefaultNone)]
// pub enum CButtonBoxAction {
//     Change(bool),
//     None,
// }

// #[derive(Live, LiveHook, LiveRegister)]
// #[repr(C)]
// struct DrawLabelText {
//     #[deref]
//     draw_super: DrawText,
//     #[live]
//     hover: f32,
//     #[live]
//     pressed: f32,
// }
// impl CustomButton {
//     pub fn draw_walk(&mut self, cx: &mut Cx2d, walk: Walk) {
//         self.draw_cbutt.begin(cx, walk, self.layout);
//         self.draw_text
//             .draw_walk(cx, self.label_walk, self.label_align, self.text.as_ref());
//         self.draw_icon.draw_walk(cx, self.icon_walk);
//         self.draw_cbutt.end(cx);
//     }
// }

// impl Widget for CustomButton {
//     fn widget_to_data(
//         &self,
//         _cx: &mut Cx,
//         actions: &Actions,
//         nodes: &mut LiveNodeVec,
//         path: &[LiveId],
//     ) -> bool {
//         match actions.find_widget_action_cast(self.widget_uid()) {
//             CButtonBoxAction::Change(v) => {
//                 nodes.write_field_value(path, LiveValue::Bool(v));
//                 true
//             }
//             _ => false,
//         }
//     }

//     fn data_to_widget(&mut self, cx: &mut Cx, nodes: &[LiveNode], path: &[LiveId]) {
//         if let Some(value) = nodes.read_field_value(path) {
//             if let Some(value) = value.as_bool() {
//                 self.animator_toggle(cx, value, Animate::Yes, id!(selected.on), id!(selected.off));
//             }
//         }
//     }

//     fn handle_event(&mut self, cx: &mut Cx, event: &Event, scope: &mut Scope) {
//         let uid = self.widget_uid();
//         self.animator_handle_event(cx, event);

//         match event.hits(cx, self.draw_cbutt.area()) {
//             Hit::FingerHoverIn(_) => {
//                 cx.set_cursor(MouseCursor::Hand);
//                 self.animator_play(cx, id!(hover.on));
//             }
//             Hit::FingerHoverOut(_) => {
//                 self.animator_play(cx, id!(hover.off));
//             }
//             Hit::FingerDown(_fe) => {
//                 if self.animator_in_state(cx, id!(selected.on)) {
//                     self.animator_play(cx, id!(selected.off));
//                     cx.widget_action(uid, &scope.path, CButtonBoxAction::Change(false));
//                 } else {
//                     self.animator_play(cx, id!(selected.on));
//                     cx.widget_action(uid, &scope.path, CButtonBoxAction::Change(true));
//                 }
//             }
//             Hit::FingerUp(_fe) => {}
//             Hit::FingerMove(_fe) => {}
//             _ => (),
//         }
//     }

//     fn draw_walk(&mut self, cx: &mut Cx2d, _scope: &mut Scope, walk: Walk) -> DrawStep {
//         self.draw_walk(cx, walk);
//         DrawStep::done()
//     }

//     fn text(&self) -> String {
//         self.text.as_ref().to_string()
//     }

//     fn set_text(&mut self, v: &str) {
//         self.text.as_mut_empty().push_str(v);
//     }
// }
// impl CustomButtonRef {
//     pub fn changed(&self, actions: &Actions) -> Option<bool> {
//         if let CButtonBoxAction::Change(b) = actions.find_widget_action_cast(self.widget_uid()) {
//             return Some(b);
//         }
//         None
//     }

//     pub fn set_text(&self, text: &str) {
//         if let Some(mut inner) = self.borrow_mut() {
//             let s = inner.text.as_mut_empty();
//             s.push_str(text);
//         }
//     }

//     pub fn selected(&self, cx: &Cx) -> bool {
//         if let Some(inner) = self.borrow() {
//             inner.animator_in_state(cx, id!(selected.on))
//         } else {
//             false
//         }
//     }

//     pub fn set_selected(&self, cx: &mut Cx, value: bool) {
//         if let Some(mut inner) = self.borrow_mut() {
//             inner.animator_toggle(cx, value, Animate::Yes, id!(selected.on), id!(selected.off));
//         }
//     }
// }
// // impl CustomButton {
// //     pub fn area(&self) -> Area {
// //         self.area
// //     }
// // }
