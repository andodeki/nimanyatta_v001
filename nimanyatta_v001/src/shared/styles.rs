use makepad_widgets::*;

live_design! {
    TITLE_TEXT = {
        font_size: (14),
        font: {path: dep("crate://makepad-widgets/resources/GoNotoKurrent-Regular.ttf")}
    }

    REGULAR_TEXT = {
        font_size: (12),
        font: {path: dep("crate://makepad-widgets/resources/GoNotoKurrent-Regular.ttf")}
    }

    TEXT_SUB = {
        font_size: (FONT_SIZE_SUB),
        font: {path: dep("crate://makepad-widgets/resources/GoNotoKurrent-Regular.ttf")}
    }
    H3_TEXT_REGULAR = {
        font_size: 9.0,
        font: {path: dep("crate://makepad-widgets/resources/IBMPlexSans-Text.ttf")}
    }

    LOGO_MAKEPAD = dep("crate://self/resources/makepad.png")
    // LOGO_MAKEPAD = dep("crate://self/resources/makepad.svg")

    CARD_CORNER_RADIUS = 20.
    CARD_HEIGHT = 550
    COLOR_PROFILE_CIRCLE = #xfff8ee
    // COLOR_DIVIDER = #x00000018
    COLOR_DIVIDER_DARK = #x00000044

    FONT_SIZE_H2 = 9.5

    HEIGHT_AUDIOVIZ = 150

    SSPACING_0 = 0.0
    SSPACING_1 = 4.0
    SSPACING_2 = (SSPACING_1 * 2)
    SSPACING_3 = (SSPACING_1 * 3)
    SSPACING_4 = (SSPACING_1 * 4)

    SPACING_0 = {top: (SSPACING_0), right: (SSPACING_0), bottom: (SSPACING_0), left: (SSPACING_0)}
    SPACING_1 = {top: (SSPACING_1), right: (SSPACING_1), bottom: (SSPACING_1), left: (SSPACING_1)}
    SPACING_2 = {top: (SSPACING_2), right: (SSPACING_2), bottom: (SSPACING_2), left: (SSPACING_2)}
    SPACING_3 = {top: (SSPACING_3), right: (SSPACING_3), bottom: (SSPACING_3), left: (SSPACING_3)}
    SPACING_4 = {top: (SSPACING_4), right: (SSPACING_4), bottom: (SSPACING_4), left: (SSPACING_4)}
    H2_TEXT_BOLD = {
        font_size: (FONT_SIZE_H2),
        font: {path: dep("crate://makepad-widgets/resources/IBMPlexSans-SemiBold.ttf")}
    }
    H2_TEXT_REGULAR = {
        font_size: (FONT_SIZE_H2),
        font: {path: dep("crate://makepad-widgets/resources/IBMPlexSans-Text.ttf")}
    }

    COLOR_DOWN_FULL = #000

    // COLOR_DOWN_0 = #x00000000
    // COLOR_DOWN_0 = #x00bbd3
    COLOR_DOWN_0 = #x1c1c1c
    COLOR_DOWN_1 = #x00000011
    COLOR_DOWN_2 = #x00000022
    COLOR_DOWN_3 = #x00000044
    COLOR_DOWN_4 = #x00000066
    // COLOR_DOWN_4 = #016def
    // COLOR_DOWN_4 = #x086975  
    COLOR_DOWN_5 = #x000000AA
    COLOR_DOWN_6 = #x000000CC
    COLOR_DOWN_7 = #x00bbd3

    COLOR_UP_0 = #xFFFFFF00
    COLOR_UP_1 = #xFFFFFF0A
    COLOR_UP_2 = #xFFFFFF10
    COLOR_UP_3 = #xFFFFFF20
    COLOR_UP_4 = #xFFFFFF40
    COLOR_UP_5 = #xFFFFFF66
    COLOR_UP_6 = #xFFFFFFCC
    COLOR_UP_FULL = #xFFFFFFFF

    COLOR_ALERT = #xFF0000FF
    COLOR_OSC = #xFFFF99FF
    COLOR_ENV = #xF9A894
    COLOR_FILTER = #x88FF88
    COLOR_FX = #x99EEFF
    COLOR_DEFAULT = (COLOR_UP_6)

    COLOR_VIZ_1 = (COLOR_DOWN_2)
    COLOR_VIZ_2 = (COLOR_DOWN_6)
    COLOR_DIVIDER = (COLOR_DOWN_5)

    // COLOR_BG = #xfff8ee
    // COLOR_BRAND = #xf88
    // COLOR_BRAND_HOVER = #xf66
    // COLOR_META_TEXT = #xaaa
    // COLOR_META = #xccc
    // COLOR_META_INV = #xfffa
    // COLOR_OVERLAY_BG = #x000000d8
    // COLOR_DIVIDER = #x00000018
    // COLOR_DIVIDER_DARK = #x00000044
    // COLOR_PROFILE_CIRCLE = #xfff8ee
    // COLOR_P = #x999
}
