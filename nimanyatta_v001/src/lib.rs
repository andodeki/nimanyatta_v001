pub use makepad_widgets;
pub mod app;
pub mod default_choice;
pub mod five;
pub mod four;
pub mod home;
pub mod shared;
pub mod three;
pub mod two;

// #[cfg(not(target_arch = "wasm32"))]
pub mod utils;
