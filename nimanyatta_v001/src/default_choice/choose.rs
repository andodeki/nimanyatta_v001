use makepad_widgets::*;
const CHOICE_MAX_OFFSET: f64 = 400.0;

live_design! {
    import makepad_draw::shader::std::*;
    import makepad_widgets::base::*;
    import makepad_widgets::theme_desktop_dark::*;
    import crate::shared::styles::*;
    import crate::shared::custom_button::CustomButton;

    IndicatorCheckBox = <CheckBox> {
        width: Fill,
        height: 35,
        margin: {left: 1},
        label_walk: {margin: {top: 15}}
        draw_check: {
            uniform size: 3.5;
            instance open: 0.0
            uniform length: 3.0
            uniform width: 1.0

            fn pixel(self) -> vec4 {
                let sdf = Sdf2d::viewport(self.pos * self.rect_size)
                match self.check_type {
                    CheckType::Check => {
                        let sz = self.size;
                        let left = sz + 1.;
                        let up = sz + 7;
                        let c = self.rect_size * vec2(0.5, 0.5);
                        sdf.box(
                            left,
                            c.y - up,
                            25. * sz + 5,
                            1.8 * sz,
                            1.8
                        );

                        sdf.fill(#232323);
                        sdf.stroke(#000, 0.5 + 0.5 * self.dpi_dilate);

                        let isz = sz * 0.5;
                        let ileft = isz + 3;
                        let iup = sz + 7;
                        sdf.box(
                            ileft,
                            c.y - iup,
                            25. * sz + 5,
                            1.8 * sz,
                            1.8
                        );
                        sdf.fill(mix(#fff0, #016def, self.selected));
                    }
                }
                return sdf.result
            }
        }
        draw_text: {text_style: <THEME_FONT_LABEL> {}}
    }
    // Indicators = {{Indicators}}{
    //     Fit,
    //     flow: Right,
    //     spacing: 5,
    //     align: {x: 0.5, y: 0.5},
    //     indicator_titles: []
    //     template: <IndicatorCheckBox> {
    //         width: 100, height: 20,
    //         margin: 0, padding: 0
    //         flow: Right,
    //         align: {x: 0.5, y: 0.5},
    //         text: "Here",
    //         draw_text: {
    //                 fn get_color(self) -> vec4 {
    //                     return #f60;
    //                 }
    //             }
    //         draw_check: {
    //             check_type: Check,
    //         }
    //     }
    // }

    Choose = {{Choose}}{
        height: Fill, width: Fill
        // flow: Down
        <Indicators>{
            // indicator_titles: [
            //     "Step One",
            //     "Step Two",
            //     "Step Three"
            // ]
        }
        <Steps>{}
    }
}

#[derive(Live, LiveHook, Widget)]
pub struct Choose {
    #[deref]
    view: View,
}
impl Widget for Choose {
    fn draw_walk(&mut self, cx: &mut Cx2d, scope: &mut Scope, walk: Walk) -> DrawStep {
        self.view.draw_walk_all(cx, scope, walk);
        DrawStep::done()
    }

    fn handle_event(&mut self, cx: &mut Cx, event: &Event, scope: &mut Scope) {
        self.view.handle_event(cx, event, scope);
    }
}

#[derive(Live, Widget)]
pub struct Indicators {
    #[walk]
    walk: Walk,
    #[redraw]
    #[live]
    draw_bg: DrawQuad,
    #[layout]
    layout: Layout,
    #[live]
    indicator_titles: Vec<String>,
    #[live]
    template: Option<LivePtr>,
    #[rust]
    items: ComponentMap<LiveId, WidgetRef>,
}
#[derive(Live, Widget)]
pub struct Steps {
    #[walk]
    walk: Walk,
    #[redraw]
    #[live]
    draw_bg: DrawQuad,
    // #[redraw] #[rust] area:Area,
    // #[walk] walk:Walk,
    #[live]
    page_titles: Vec<String>,
    #[live]
    choices_values: Vec<Vec<String>>,
    #[rust]
    items: ComponentMap<LiveId, WidgetRef>,
    #[live]
    page_template: Option<LivePtr>,
    #[live]
    options_template: Option<LivePtr>,
    #[rust(0)]
    current_page: u8,
    #[rust]
    pages: ComponentMap<LiveId, WidgetRef>,
    #[live]
    choice_page_offset: f64,
    #[animator]
    animator: Animator,
}

impl LiveHook for Indicators {
    fn after_apply(&mut self, cx: &mut Cx, _apply: &mut Apply, _index: usize, _nodes: &[LiveNode]) {
        // let tags = ["test1", "test2", "test3"];
        self.items.clear();
        for (i, title_text) in self.indicator_titles.iter().enumerate() {
            // for (i, title_text) in tags.iter().enumerate() {
            let item_id = LiveId::from_str(&format!("items{}", i));
            let item_widget = WidgetRef::new_from_ptr(cx, self.template);
            // item_widget.apply_over(cx, live! {label = { text: (title_text) }});
            item_widget.apply_over(cx, live! {text: (title_text) });
            self.items.insert(item_id, item_widget);
        }
    }
}

impl Widget for Indicators {
    fn handle_event(&mut self, cx: &mut Cx, event: &Event, scope: &mut Scope) {
        for (_id, item) in self.items.iter_mut() {
            item.handle_event(cx, event, scope);
        }
    }

    fn draw_walk(&mut self, cx: &mut Cx2d, scope: &mut Scope, walk: Walk) -> DrawStep {
        cx.begin_turtle(walk, self.layout);
        //Indicators
        for (_id, item) in self.items.iter_mut() {
            let _ = item.draw_all(cx, scope);
        }

        cx.end_turtle_with_area(&mut self.draw_bg.draw_vars.area);
        DrawStep::done()
    }
}

impl LiveHook for Steps {
    // fn after_new_from_doc(&mut self, cx: &mut Cx) {
    fn after_apply(&mut self, cx: &mut Cx, _apply: &mut Apply, _index: usize, _nodes: &[LiveNode]) {
        for (idx, title_text) in self.page_titles.iter().enumerate() {
            let widget_id = LiveId::from_str(&format!("page{}", idx));
            let page = self.pages.get_or_insert(cx, widget_id, |cx| {
                WidgetRef::new_from_ptr(cx, self.page_template)
            });

            page.label(id!(step_title))
                .set_text_and_redraw(cx, &format!("{}", &title_text.as_str()))
        }
        for (idx, title_text_vec) in self.choices_values.iter().enumerate() {
            log!("Options: {:?}", &title_text_vec);
            for (_idx, title_text) in title_text_vec.iter().enumerate() {
                log!("Options: {:?}", &title_text);
                let widget_id = LiveId::from_str(&format!("page{}", idx));
                let page = self.pages.get_or_insert(cx, widget_id, |cx| {
                    WidgetRef::new_from_ptr(cx, self.page_template)
                });
                page.button(id!(but))
                    .set_text_and_redraw(cx, &format!("{}", &title_text.as_str()))
            }
        }
    }
}

impl Widget for Steps {
    fn handle_event(&mut self, cx: &mut Cx, event: &Event, _scope: &mut Scope) {
        // //Options
        // for (_id, item) in self.items.iter_mut() {
        //     item.handle_event(cx, event, scope);
        // }
        if self.animator_handle_event(cx, event).must_redraw() {
            self.redraw(cx);
        }

        // Fire the "show" animation when the "restart" animation is done
        if self.animator.animator_in_state(cx, id!(choice.restart)) {
            self.animator_play(cx, id!(choice.show));
        }

        match event.hits(cx, self.draw_bg.draw_vars.area) {
            Hit::FingerUp(fe) => {
                if fe.is_over {
                    // Do not fire a new animation if the choice is already animating
                    if !self.animator.is_track_animating(cx, id!(choice)) {
                        self.update_current_page();
                        self.animator_play(cx, id!(choice.restart));
                        //self.redraw(cx);
                    }
                }
            }
            _ => (),
        };
    }
    // fn redraw(&mut self, cx: &mut Cx) {
    //     self.area.redraw(cx);
    // }

    fn draw_walk(&mut self, cx: &mut Cx2d, scope: &mut Scope, walk: Walk) -> DrawStep {
        // let walk = self.walk(cx);
        self.draw_walkd(cx, scope, walk);
        DrawStep::done()
    }
}

impl Steps {
    fn draw_walkd(&mut self, cx: &mut Cx2d, scope: &mut Scope, walk: Walk) {
        cx.begin_turtle(walk, Layout::default());

        self.draw_page_with_offset(cx, scope, self.current_page, self.choice_page_offset, walk);

        let prev_page_idx =
            (self.current_page + self.page_titles.len() as u8 - 1) % self.page_titles.len() as u8;
        self.draw_page_with_offset(
            cx,
            scope,
            prev_page_idx,
            self.choice_page_offset - CHOICE_MAX_OFFSET,
            walk,
        );

        cx.end_turtle_with_area(&mut self.draw_bg.draw_vars.area);
    }

    fn draw_page_with_offset(
        &mut self,
        cx: &mut Cx2d,
        scope: &mut Scope,
        index: u8,
        offset: f64,
        walk: Walk,
    ) {
        let widget_id = LiveId::from_str(&format!("page{}", index));
        let page = self.pages.get_or_insert(cx, widget_id, |cx| {
            WidgetRef::new_from_ptr(cx, self.page_template)
        });

        let _ = page.draw_walk(cx, scope, walk.with_margin_left(offset));
    }

    fn update_current_page(&mut self) {
        self.current_page = (self.current_page + 1) % self.page_titles.len() as u8;
        // self.current_page = (self.current_page + 1) % self.choices_values.len() as u8;
    }
}
