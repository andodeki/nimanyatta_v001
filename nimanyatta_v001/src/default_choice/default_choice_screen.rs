use makepad_widgets::widget::WidgetCache;
use makepad_widgets::*;

live_design! {
    import makepad_draw::shader::std::*;
    import makepad_widgets::base::*;
    import makepad_widgets::theme_desktop_dark::*;

    import crate::shared::styles::*;
    import crate::shared::custom_button::CustomButton;
    import crate::shared::round_slider::RoundSlider;
    // import crate::default_choice::choose::Choose;Choose
    import crate::default_choice::choice::Choose;


    StepOneScreen = <View> {
        flow: Down,
        spacing: (SSPACING_2),
        align: {x: 0.5, y: 0.5}
        label = <Label> {
            margin: {top: 1}
            draw_text: {
                text_style: <H2_TEXT_BOLD> {},
                color: (COLOR_DOWN_6)
            }
            text: "Step One"
        }
        getstarted = <CustomButton> {
            width: 250
            height: Fit
            text: "Submit"
            draw_bg: {
                border_radius: 2.
            }
        }
        getstartedw = <CustomButton> {
            width: 250
            height: Fit
            text: "Submit"
            draw_bg: {
                border_radius: 2.
            }
        }
    }
    StepTwoScreen = <View> {
        flow: Down, spacing: (SSPACING_2),

        align: {x: 0.5, y: 0.5}
        label = <Label> {
            margin: {top: 1}
            draw_text: {
                text_style: <H2_TEXT_BOLD> {},
                color: (COLOR_DOWN_6)
            }
            text: "Step Two"
        }
        getstarted = <CustomButton> {
            width: 250
            height: Fit
            text: "Submit"
            draw_bg: {
                border_radius: 2.
            }
        }
    }
    StepThreeScreen = <View> {
        flow: Down, spacing: (SSPACING_2),

        align: {x: 0.5, y: 0.5}
        label = <Label> {
            margin: {top: 1}
            draw_text: {
                text_style: <H2_TEXT_BOLD> {},
                color: (COLOR_DOWN_6)
            }
            text: "Step Three"
        }
        getstarted = <CustomButton> {
            width: 250
            height: Fit
            text: "Submit"
            draw_bg: {
                border_radius: 2.
            }
        }
    }

    FishHeader = <RoundedView> {
        flow: Right
        height: Fit,
        width: Fill,
        margin: {bottom: (SSPACING_2)}
        align: {x: 1.0, y: 0.5},
        // flow: Right,
        // spacing: 5.,
        padding: {right: 5.}


        // title = <FishTitle> {
        //     height: Fit,
        //     width: Fill,
        //     margin: <SPACING_0> {}
        //     padding: <SPACING_2> {}
        // }
        menu = <DropDown> {//DropDown  NavBar

        }
    }

    Indicator = <RoundedView> {
        width: 100.0
        height: 8.0
        draw_bg: {
            color: #f60,
            radius: 2.5
        }
    }
    DefaultChoiceScreen = {{DefaultChoiceScreen}} {
        width: Fit,
        height: Fit,
        flow: Right,
        spacing: 5,

        template: <CheckBox> {}
        // template: <Indicator> {}
    }
    DefaultChoice = <View> {
        width: Fill, height: Fill flow: Down
        <View> {
            flow: Down,
            align: {x: 0.5, y: 0.3}
            //     <ModelFilesTags>{}
            // // <DefaultChoiceScreen>{}

            <View> {
                flow: Down,
                spacing: (SSPACING_2),
                align: {x: 0.5, y: 0.5}

                // <DefaultChoiceScreen>{}
                Choose = <Choose> {}
                // <View> {
                //     debug: M
                //     width: 300, height: 150
                //     align: {x: 0.5, y: 0.5}
                //     <RoundSlider> {}
                // }

                // label = <Label> {
                //     margin: {top: 1}
                //     draw_text: {
                //         text_style: <H2_TEXT_BOLD> {},
                //         color: (COLOR_DOWN_6)
                //     }
                //     text: "Step One"
                // }
                // getstarted = <CustomButton> {
                //     width: 250
                //     height: Fit
                //     text: "Submit"
                //     draw_bg: {
                //         border_radius: 2.
                //     }
                // }
                // getstartedw = <CustomButton> {
                //     width: 250
                //     height: Fit
                //     text: "Submit"
                //     draw_bg: {
                //         border_radius: 2.
                //     }
                // }
            }
        }
    }
}

#[derive(Live, Widget)]
pub struct DefaultChoiceScreen {
    #[deref]
    view: View,

    #[redraw]
    #[rust]
    area: Area,

    #[walk]
    walk: Walk,

    #[layout]
    layout: Layout,

    #[live]
    template: Option<LivePtr>,

    #[rust]
    items: ComponentMap<LiveId, WidgetRef>,
}

impl LiveHook for DefaultChoiceScreen {
    fn after_apply(&mut self, cx: &mut Cx, _apply: &mut Apply, _index: usize, _nodes: &[LiveNode]) {
        let tags = ["test1", "test2", "test3"];

        self.items.clear();
        for (i, tag) in tags.iter().enumerate() {
            let item_id = LiveId(i as u64).into();
            let item_widget = WidgetRef::new_from_ptr(cx, self.template);
            item_widget.apply_over(cx, live! {label = { text: (tag) }});
            self.items.insert(item_id, item_widget);
        }
    }
}

impl Widget for DefaultChoiceScreen {
    fn handle_event(&mut self, cx: &mut Cx, event: &Event, scope: &mut Scope) {
        let _uid = self.widget_uid();

        let actions = cx.capture_actions(|cx| self.view.handle_event(cx, event, scope));
        if self.view.button(id!(getstartedw)).clicked(&actions) {
            log!("Option Selected: {}", 0);
        }
        for (_id, item) in self.items.iter_mut() {
            item.handle_event(cx, event, scope);
        }
    }

    fn draw_walk(&mut self, cx: &mut Cx2d, scope: &mut Scope, walk: Walk) -> DrawStep {
        cx.begin_turtle(walk, self.layout);
        for (_id, item) in self.items.iter_mut() {
            let _ = item.draw_all(cx, scope);
        }
        cx.end_turtle_with_area(&mut self.area);
        DrawStep::done()
    }
}
