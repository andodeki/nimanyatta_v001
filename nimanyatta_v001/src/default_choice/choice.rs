// use makepad_widgets::*;
use crate::makepad_widgets::*;
const CHOICE_MAX_OFFSET: f64 = 700.0;
const SELECT_MAX_OFFSET: f64 = 500.0;
// use crate::shared::custom_button::CustomButton;

live_design! {
    import makepad_draw::shader::std::*;
    import makepad_widgets::base::*;
    import makepad_widgets::theme_desktop_dark::*;
    import crate::shared::styles::*;
    import crate::shared::steps::StepNav;
    import crate::shared::custom_button::CustomButton;

    Indicator = <RoundedView> {
        width: 100.0
        height: 8.0
        draw_bg: {
            color: #f60,
            radius: 2.5
        }
    }
    ButChoices1 = <View>{
        flow: Down,
        show_bg: false
        draw_bg: {
            color: #000
        }
        align: {x: 0.5, y: 0.5}
        but = <CustomButton> {
            width: 300, height: 45,
            text: "Option 1"
            draw_bg: {
                border_radius: 2.
            }
        }
    }
    ButChoices2 = <View>{
        flow: Down,
        show_bg: false
        draw_bg: {
            color: #000
        }
        align: {x: 0.5, y: 0.5}
        but = <RadioButton> {
            width: 300, height: 45,
            text: "Option 1"
            // draw_bg: {
            //     border_radius: 2.
            // }
        }
    }

    SIDEBAR_FONT_COLOR = #344054
    SIDEBAR_FONT_COLOR_HOVER = #344054
    SIDEBAR_FONT_COLOR_SELECTED = #127487

    SIDEBAR_BG_COLOR = #f
    SIDEBAR_BG_COLOR_HOVER = #E2F1F199
    SIDEBAR_BG_COLOR_SELECTED = #E2F1F199

    ButChoices = <ButtonGroup>{
        // debug: A
        flow: Down,
        show_bg: false
        draw_bg: {
            color: #000
        }
        align: {x: 0.5, y: 0.5}
        but = <View> {
            // debug: A
            width: 300, height: 45,
            butchoice = <RadioButtonTextual> {
                width: Fill,
                height: 70,
                align: {x: 0.5, y: 0.5}

                text: "Option 1"
                label_walk: { width: Fit, height: 34.0 }
                icon_walk: {width: 80, height: 80}

                draw_radio: {
                    radio_type: Tab,

                    instance border_width: 0.0
                    instance border_color: #344054
                    instance inset: vec4(0.0, 0.0, 0.0, 0.0)
                    instance radius: 2.5

                    fn get_color(self) -> vec4 {
                        return mix(
                            mix(
                                (SIDEBAR_BG_COLOR),
                                (SIDEBAR_BG_COLOR_HOVER),
                                self.hover
                            ),
                            (SIDEBAR_BG_COLOR_SELECTED),
                            self.selected
                        )
                    }

                    fn get_border_color(self) -> vec4 {
                        return self.border_color
                    }

                    fn pixel(self) -> vec4 {
                        let border_color = #0157c0;
                        let border_width = 0.5;

                        let sdf = Sdf2d::viewport(self.pos * self.rect_size)
                        sdf.box(
                            self.inset.x + self.border_width,
                            self.inset.y + self.border_width,
                            self.rect_size.x - (self.inset.x + self.inset.z + self.border_width * 2.0),
                            self.rect_size.y - (self.inset.y + self.inset.w + self.border_width * 2.0),
                            max(1.0, self.radius)
                        )
                        sdf.fill_keep(self.get_color())
                        if self.border_width > 0.0 {
                            sdf.stroke(self.get_border_color(), self.border_width)
                        }
                        sdf.stroke(
                            border_color,
                            border_width
                        )
                        return sdf.result;
                    }
                }

                draw_text: {
                    color_unselected: (SIDEBAR_FONT_COLOR)
                    color_unselected_hover: (SIDEBAR_FONT_COLOR_HOVER)
                    color_selected: (SIDEBAR_FONT_COLOR_SELECTED)

                    fn get_color(self) -> vec4 {
                        return mix(
                            mix(
                                self.color_unselected,
                                self.color_unselected_hover,
                                self.hover
                            ),
                            self.color_selected,
                            self.selected
                        )
                    }
                }
            }
        }
    }
    // <ButtonGroup> {
    //     height: Fit
    //     flow: Right
    //     align: { x: 0.0, y: 0.5 }
    //     radiotabs_demo = <View> {
    //         width: Fit, height: Fit,
    //         radio1 = <RadioButtonTab> { label: "Option 1" }
    //         radio2 = <RadioButtonTab> { label: "Option 2" }
    //         radio3 = <RadioButtonTab> { label: "Option 3" }
    //         radio4 = <RadioButtonTab> { label: "Option 4" }
    //     }
    // }
    SelectOptions = {{SelectOptions}} {
        // page_titles: []
        width: Fit, height: Fit,
        // indicator_titles: []
        flow: Down,
        spacing: 10,
        align: {x: 0.5, y: 0.5},
        select_choices_values: []
        selects_template: <ButChoices> {
            flow: Down,
        }
        choice_page_offset: 0.0
        animator: {
            selectopt = {
                default: restart,
                restart = {
                    ease: ExpDecay {d1: 0.80, d2: 0.97}
                    from: {all: Snap}
                    apply: {choice_page_offset: 500.0}
                }
                show = {
                    redraw: true,
                    ease: ExpDecay {d1: 0.80, d2: 0.97}
                    // from: {all: Forward {duration: 0}}
                    from: {all: Snap}
                    apply: {choice_page_offset: 0.0}
                }
            }
        }

    }
    OptionC = {{Indicators}} {
        width: Fit,
        height: Fit,
        flow: Down,
        spacing: 5,
        align: {x: 0.5, y: 0.5},
        indicator_titles: []
        template: <CustomButton> {
            width: 100, height: Fit,
            // margin: 0, padding: 0
            text: "Option 1"
            draw_bg: {
                border_radius: 2.
            }
        }
    }
    StepsScreen = <View> {
        // debug: A
        // width: 300,
        height: Fit,
        flow: Down,
        // padding: 10.0
        // align: {y: 0.5}
        align: {y: 0.5}
        // padding: 20,
        spacing: (THEME_SPACE_2),


        title = <View> {
            // debug: A
            // align: {x: 0.1}
            // padding: {left: 10.0},
            height: Fit
            spacing: 0,
            step_title = <Label> {
                // margin: {top: 10.0, left: 10.0}
                draw_text: {
                    // text_style: <H2_TEXT_BOLD> {},
                    text_style: <REGULAR_TEXT>{font_size: 12.},
                    color: (COLOR_DOWN_6)
                }
                text: "Step One"
            }
        }
        // <ButChoices> {}
        avail_options = <View> {
            // debug: A
            align: {x: 0.5, y: 0.5}
            height: Fit
            <SelectOptions> {
                // select_choices_values: [
                //     ["STP1-Option 1","STP1-Option 2","STP1-Option 3"],
                //     ["STP2-Option 1", "STP2-Option 2", "STP2-Option 3", "STP2-Option 4"],
                //     ["STP3-Option 1", "STP3-Option 2", "STP3-Option 3", "STP3-Option 4", "STP3-Option 5"]
                // ]
                select_choices_values: []
            }
        }

    }
    IndicatorCheckBox = <CheckBox> {
        width: Fill,
        height: 35,
        margin: {left: 1},
        label_walk: {margin: {top: 15, bottom: 10}}
        draw_check: {
            uniform size: 3.5;
            instance open: 0.0
            uniform length: 3.0
            uniform width: 1.0

            fn pixel(self) -> vec4 {
                let sdf = Sdf2d::viewport(self.pos * self.rect_size)
                match self.check_type {
                    CheckType::Check => {
                        let sz = self.size;
                        let left = sz + 1.;
                        let up = sz + 7;
                        let c = self.rect_size * vec2(0.5, 0.5);
                        sdf.box(
                            left,
                            c.y - up,
                            25. * sz + 5,
                            1.8 * sz,
                            1.8
                        );

                        sdf.fill(#232323);
                        sdf.stroke(#000, 0.5 + 0.5 * self.dpi_dilate);

                        let isz = sz * 0.5;
                        let ileft = isz + 3;
                        let iup = sz + 7;
                        sdf.box(
                            ileft,
                            c.y - iup,
                            25. * sz + 5,
                            1.8 * sz,
                            1.8
                        );
                        sdf.fill(mix(#fff0, #016def, self.selected));
                    }
                }
                return sdf.result
            }
        }
        draw_text: {text_style: <THEME_FONT_LABEL> {}}
    }
    Indicators = {{Indicators}} {
        width: Fit,
        height: Fit,
        flow: Right,
        // flow: Down,
        spacing: 5,
        // align: {x: 0.5, y: 0.5},
        indicator_titles: []
        template: <IndicatorCheckBox> {
            width: 100, height: 20,
            margin: 0, padding: 0
            // flow: Right,
            // flow: Down,
            align: {x: 0.5, y: 0.5},
            text: "Here",
            draw_text: {
                    fn get_color(self) -> vec4 {
                        return #f60;
                    }
                }
            draw_check: {
                check_type: Check,
            }
        }
    }
    Steps = {{Steps}} {
        // debug: A
        page_titles: []
        choices_values: []

        page_template: <StepsScreen> { }
        // custom_button: <CustomButton>{
        //     width: Fill, height: Fit,
        //     text: "Next"
        //     draw_bg: {
        //         border_radius: 2.
        //     }
        // }
        choice_page_offset: 0.0
        animator: {
            choice = {
                default: restart,
                restart = {
                    from: {all: Snap}
                    apply: {choice_page_offset: 400.0}
                }
                show = {
                    redraw: true,
                    from: {all: Forward {duration: 0.5}}
                    apply: {choice_page_offset: 0.0}
                }
            }
        }
    }
    Choose = {{Choose}}{
        // debug: A
        width: 400,
        // height: 500,
        height: 488,
        // height: Fit,
        padding: 20, spacing: 20
        align: {x: 0.5, y: 0.5}
        <View>{
            // debug: A
            flow: Down,
            width: Fit,
            // height: Fill,
            // spacing: 20
            align: {y: 0.5}
            <View>{
                // debug: A
                height: Fit,
                width: Fit,
                margin: {bottom: 20.0}
                <Indicators>{
                    indicator_titles: [
                        "Step One",
                        "Step Two",
                        "Step Three"
                    ]
                }
            }
            back = <View> {
                // debug: A
                width: Fit, height: Fit,
                back_left_button = <Button> {
                    width: Fit, height: Fit,
                    icon_walk: {width: 10, height: Fit}
                    text: "Back"
                    draw_text: {
                        text_style: <REGULAR_TEXT>{font_size: 12.},
                        fn get_color(self) -> vec4 {
                            // return #016def
                            return mix(mix(#000, #000, self.hover), #000, self.pressed)
                        }
                    }
                    draw_bg: {
                        fn pixel(self) -> vec4 {
                            let sdf = Sdf2d::viewport(self.pos * self.rect_size);
                            return sdf.result
                        }
                    }
                    draw_icon: {
                        svg_file: dep("crate://self/resources/back.svg"),
                        // color: (THEME_COLOR_TEXT_DEFAULT);
                        color: #000;
                        brightness: 0.8;
                    }
                }
            }
            <Steps>{
                page_template: <StepsScreen> {}
                page_titles: ["Step One", "Step Two", "Step Three" ]
                choices_values: [
                    ["STP1-Option 1"],
                    ["STP2-Option 1", "STP2-Option 2", "STP2-Option 3"],
                    ["STP3-Option 1", "STP3-Option 2"]
                ]
            }
            next_button = <View> {
                // debug: A
                height: Fit
                align: {x: 0.5, y: 0.5}
                next = <CustomButton> {
                    // debug: A
                    width: 300, height: 50,
                    // margin: 0, padding: 0
                    text: "Next"
                    draw_text: {
                        fn get_color(self) -> vec4 {
                            // return #016def // 0160C0
                            return mix(mix(#f0, #016def, self.hover), #f0, self.pressed)
                        }
                    }
                    draw_bg: {
                        border_radius: 2.
                        fn pixel(self) -> vec4 {
                            let border_color = #0157c0;
                            // let border_color = #016def; //#0157c0
                            let border_width = 0.5;
                            let sdf = Sdf2d::viewport(self.pos * self.rect_size);
                            let body = mix(mix(#0157c0, #f, self.hover), #0157c0, self.pressed);

                            sdf.box(
                                1.,
                                1.,
                                self.rect_size.x - 2.0,
                                self.rect_size.y - 2.0,
                                self.border_radius
                            )
                            sdf.fill_keep(body)

                            sdf.stroke(
                                border_color,
                                border_width
                            )
                            return sdf.result
                        }
                    }
                }
            }
        }

    }

}
#[derive(Live, LiveHook, Widget)]
pub struct Choose {
    #[deref]
    view: View,
    #[animator]
    animator: Animator,
    #[rust]
    screen_width: f64,
}
impl Widget for Choose {
    fn draw_walk(&mut self, cx: &mut Cx2d, scope: &mut Scope, walk: Walk) -> DrawStep {
        self.view.draw_walk_all(cx, scope, walk);
        // log!("Choose: draw_walk");

        DrawStep::done()
    }

    fn handle_event(&mut self, cx: &mut Cx, event: &Event, scope: &mut Scope) {
        self.view.handle_event(cx, event, scope);
    }
}
impl WidgetMatchEvent for Choose {
    fn handle_actions(&mut self, cx: &mut Cx, actions: &Actions, _scope: &mut Scope) {
        let back_left_button_clicked = self.button(id!(back_left_button)).clicked(&actions);
        if back_left_button_clicked {
            self.next_page_view(cx);
        }
        let next_button_clicked = self.button(id!(next)).clicked(&actions);
        if next_button_clicked {
            self.next_page_view(cx);
        }
        for action in actions {
            if let WindowAction::WindowGeomChange(ce) = action.as_widget_action().cast() {
                self.screen_width = ce.new_geom.inner_size.x * ce.new_geom.dpi_factor;

                log!("SCREEN_WIDTH ===> {}", self.screen_width);
            }
        }
    }
}
impl Choose {
    fn back_left_view(&mut self, cx: &mut Cx) {
        // // Fire the "show" animation when the "restart" animation is done
        // if self.animator.animator_in_state(cx, id!(choice.restart)) {
        //     self.animator_play(cx, id!(choice.show));
        // }
        self.animator_play(cx, id!(choice.show));
        // cx.widget_action(
        //     self.widget_uid(),
        //     &HeapLiveIdPath::default(),
        //     StackNavigationTransitionAction::HideBegin,
        // );
    }
    fn next_page_view(&mut self, cx: &mut Cx) {
        // // Fire the "show" animation when the "restart" animation is done
        // if self.animator.animator_in_state(cx, id!(choice.restart)) {
        //     self.animator_play(cx, id!(choice.show));
        // }
        self.animator_play(cx, id!(choice.show));
        // cx.widget_action(
        //     self.widget_uid(),
        //     &HeapLiveIdPath::default(),
        //     StackNavigationTransitionAction::HideBegin,
        // );
    }
}
#[derive(Live, Widget)]
pub struct Indicators {
    #[redraw]
    #[rust]
    area: Area,

    #[walk]
    walk: Walk,

    #[layout]
    layout: Layout,

    #[live]
    indicator_titles: Vec<String>,

    #[live]
    template: Option<LivePtr>,

    #[rust]
    items: ComponentMap<LiveId, WidgetRef>,
}

impl LiveHook for Indicators {
    fn after_apply(&mut self, cx: &mut Cx, _apply: &mut Apply, _index: usize, _nodes: &[LiveNode]) {
        // let tags = ["test1", "test2", "test3"];
        // log!("Indicators: after_apply");

        self.items.clear();
        for (i, title_text) in self.indicator_titles.iter().enumerate() {
            // for (i, title_text) in tags.iter().enumerate() {
            let item_id = LiveId::from_str(&format!("items{}", i));
            let item_widget = WidgetRef::new_from_ptr(cx, self.template);
            // item_widget.apply_over(cx, live! {label = { text: (title_text) }});
            item_widget.apply_over(cx, live! {text: (title_text) });
            self.items.insert(item_id, item_widget);
        }
    }
}

impl Widget for Indicators {
    fn handle_event(&mut self, cx: &mut Cx, event: &Event, scope: &mut Scope) {
        for (_id, item) in self.items.iter_mut() {
            item.handle_event(cx, event, scope);
        }
    }

    fn draw_walk(&mut self, cx: &mut Cx2d, scope: &mut Scope, walk: Walk) -> DrawStep {
        cx.begin_turtle(walk, self.layout);
        //Indicators
        let mut arr: Vec<WidgetRef> = Vec::new();
        for (_idx, inner_vec) in self.items.iter_mut() {
            // Append the inner vector to the new variable
            arr.push(inner_vec.clone());
        }
        for (inneridx, indicator_arr) in arr.iter().enumerate() {
            let index: u8 = inneridx as u8;
            let widget_id = LiveId::from_str(&format!("items{}", index));
            let item = self.items.get_or_insert(cx, widget_id, |cx| {
                WidgetRef::new_from_ptr(cx, self.template)
            });
            let _ = item.draw_all(cx, scope);
        }
        // for (_id, item) in self.items.iter_mut() {
        //     // let _ = item.draw(cx, scope);
        //     let _ = item.draw_all(cx, scope);
        // }

        cx.end_turtle_with_area(&mut self.area);
        DrawStep::done()
    }
}
#[derive(Live, Widget)]
pub struct Steps {
    #[redraw]
    #[rust]
    area: Area,

    #[walk]
    walk: Walk,

    #[live]
    page_titles: Vec<String>,
    #[live]
    choices_values: Vec<Vec<String>>,

    #[rust]
    items: ComponentMap<LiveId, WidgetRef>,

    #[live]
    page_template: Option<LivePtr>,

    #[live]
    options_template: Option<LivePtr>,

    #[rust(0)]
    current_page: u8,

    #[rust]
    pages: ComponentMap<LiveId, WidgetRef>,

    #[live]
    choice_page_offset: f64,

    #[animator]
    animator: Animator,
}

impl LiveHook for Steps {
    fn after_new_from_doc(&mut self, cx: &mut Cx) {
        log!("Steps::after_new_from_doc");
        for (idx, title_text) in self.page_titles.iter().enumerate() {
            let widget_id = LiveId::from_str(&format!("page{}", idx));
            let page = self.pages.get_or_insert(cx, widget_id, |cx| {
                WidgetRef::new_from_ptr(cx, self.page_template)
            });

            page.label(id!(step_title))
                .set_text_and_redraw(cx, &format!("{}", &title_text.as_str()))
        }
    }
}

impl Widget for Steps {
    fn handle_event(&mut self, cx: &mut Cx, event: &Event, scope: &mut Scope) {
        for (_id, page) in self.pages.iter_mut() {
            page.handle_event(cx, event, scope);
        }
        if self.animator_handle_event(cx, event).must_redraw() {
            self.redraw(cx);
        }

        // Fire the "show" animation when the "restart" animation is done
        if self.animator.animator_in_state(cx, id!(choice.restart)) {
            self.animator_play(cx, id!(choice.show));
        }

        // // match event.hits(cx, self.custom_button.area()) {
        // match event.hits(cx, self.area) {
        //     Hit::FingerUp(fe) => {
        //         if fe.is_over {
        //             // Do not fire a new animation if the choice is already animating
        //             if !self.animator.is_track_animating(cx, id!(choice)) {
        //                 self.update_current_page();
        //                 self.animator_play(cx, id!(choice.restart));
        //                 //self.redraw(cx);
        //             }
        //         }
        //     }
        //     _ => (),
        // };
    }
    fn draw_walk(&mut self, cx: &mut Cx2d, scope: &mut Scope, walk: Walk) -> DrawStep {
        self.draw_walkd(cx, scope, walk);
        DrawStep::done()
    }
}
impl WidgetMatchEvent for Steps {
    fn handle_actions(&mut self, cx: &mut Cx, actions: &Actions, _scope: &mut Scope) {
        for action in actions {
            if let WindowAction::WindowGeomChange(ce) = action.as_widget_action().cast() {
                let screen_width = ce.new_geom.inner_size.x * ce.new_geom.dpi_factor;

                log!("SCREEN_WIDTH ===> {}", screen_width);
            }
        }
    }
}
impl Steps {
    fn draw_walkd(&mut self, cx: &mut Cx2d, scope: &mut Scope, walk: Walk) {
        cx.begin_turtle(walk, Layout::default());

        self.draw_page_with_offset(cx, scope, self.current_page, self.choice_page_offset, walk);

        let prev_page_idx =
            (self.current_page + self.page_titles.len() as u8 - 1) % self.page_titles.len() as u8;
        self.draw_page_with_offset(
            cx,
            scope,
            prev_page_idx,
            self.choice_page_offset - CHOICE_MAX_OFFSET,
            walk,
        );

        cx.end_turtle_with_area(&mut self.area);
        // cx.end_turtle_with_area(&mut self.custom_button.area());
    }

    fn draw_page_with_offset(
        &mut self,
        cx: &mut Cx2d,
        scope: &mut Scope,
        index: u8,
        offset: f64,
        walk: Walk,
    ) {
        let widget_id = LiveId::from_str(&format!("page{}", index));
        let page = self.pages.get_or_insert(cx, widget_id, |cx| {
            WidgetRef::new_from_ptr(cx, self.page_template)
        });

        let _ = page.draw_walk(cx, scope, walk.with_margin_left(offset));
    }

    fn update_current_page(&mut self) {
        self.current_page = (self.current_page + 1) % self.page_titles.len() as u8;
        log!("Steps::update_current_page: {:?}", &self.current_page);
        // self.current_page = (self.current_page + 1) % self.choices_values.len() as u8;
    }
}

#[derive(Live, Widget)]
pub struct SelectOptions {
    #[redraw]
    #[rust]
    area: Area,

    #[walk]
    walk: Walk,

    #[layout]
    layout: Layout,

    #[live]
    page_titles: Vec<String>,

    #[live]
    select_choices_values: Vec<Vec<String>>,

    #[live]
    selects_template: Option<LivePtr>,

    #[live]
    options_template: Option<LivePtr>,

    #[rust(0)]
    current_page: u8,

    #[rust]
    pages: ComponentMap<LiveId, WidgetRef>,

    #[live]
    choice_page_offset: f64,

    #[animator]
    animator: Animator,
}
impl LiveHook for SelectOptions {
    fn after_new_from_doc(&mut self, cx: &mut Cx) {
        log!("SelectOptions::after_new_from_doc");
        // fn after_apply(&mut self, cx: &mut Cx, _apply: &mut Apply, _index: usize, _nodes: &[LiveNode]) {
        //     log!("SelectOptions: after_apply");
        self.select_choices_values = vec![
            vec![
                "STP1-Option 1".to_string(),
                "STP1-Option 2".to_string(),
                "STP1-Option 3".to_string(),
            ],
            vec![
                "STP2-Option 1".to_string(),
                "STP2-Option 2".to_string(),
                "STP2-Option 3".to_string(),
                "STP2-Option 4".to_string(),
            ],
            vec![
                "STP3-Option 1".to_string(),
                "STP3-Option 2".to_string(),
                "STP3-Option 3".to_string(),
                "STP3-Option 4".to_string(),
                "STP3-Option 5".to_string(),
            ],
        ];

        // for (idx, title_text_vec) in self.select_choices_values[2].iter().enumerate() {
        //     let widget_id = LiveId::from_str(&format!("page{}", idx));
        //     let page = WidgetRef::new_from_ptr(cx, self.selects_template);
        //     // page.apply_over(cx, live! { label = {text:(title_text_vec)}});
        //     page.apply_over(cx, live! {label: (title_text_vec) });
        //     self.pages.insert(widget_id, page);
        // }

        for (idx, title_text_vec) in self.select_choices_values[0].iter().enumerate() {
            let widget_id = LiveId::from_str(&format!("page{}", idx));
            let page = self.pages.get_or_insert(cx, widget_id, |cx| {
                WidgetRef::new_from_ptr(cx, self.selects_template)
            });
            // page.button(id!(but))
            page.radio_button(id!(butchoice))
                // page.label(id!(but))
                .set_text_and_redraw(cx, &format!("{}", &title_text_vec.as_str()))
        }
    }
}

impl Widget for SelectOptions {
    fn handle_event(&mut self, cx: &mut Cx, event: &Event, scope: &mut Scope) {
        for (_id, page) in self.pages.iter_mut() {
            page.handle_event(cx, event, scope);
        }
        // if self.animator_handle_event(cx, event).must_redraw() {
        //     self.redraw(cx);
        // }

        // // Fire the "show" animation when the "restart" animation is done
        // if self.animator.animator_in_state(cx, id!(selectopt.restart)) {
        //     self.animator_play(cx, id!(selectopt.show));
        // }

        // // match event.hits(cx, self.custom_button.area()) {
        // match event.hits(cx, self.area) {
        //     Hit::FingerUp(fe) => {
        //         if fe.is_over {
        //             // Do not fire a new animation if the selectopt is already animating
        //             if !self.animator.is_track_animating(cx, id!(selectopt)) {
        //                 self.update_current_page();
        //                 self.animator_play(cx, id!(selectopt.restart));
        //                 // self.redraw(cx);
        //             }
        //         }
        //     }
        //     _ => (),
        // };
    }
    // fn redraw(&mut self, cx: &mut Cx) {
    //     self.area.redraw(cx);
    // }

    fn draw_walk(&mut self, cx: &mut Cx2d, scope: &mut Scope, walk: Walk) -> DrawStep {
        // let walk = self.walk(cx);
        self.draw_walkd(cx, scope, walk);
        // println!("=>{:?}", self.select_choices_values[0].clone());

        DrawStep::done()
    }
}
impl WidgetMatchEvent for SelectOptions {
    fn handle_actions(&mut self, cx: &mut Cx, actions: &Actions, _scope: &mut Scope) {
        for action in actions {
            if let WindowAction::WindowGeomChange(ce) = action.as_widget_action().cast() {
                let screen_width = ce.new_geom.inner_size.x * ce.new_geom.dpi_factor;

                log!("SCREEN_WIDTH ===> {}", screen_width);
            }
        }
    }
}
impl SelectOptions {
    fn draw_walkd(&mut self, cx: &mut Cx2d, scope: &mut Scope, walk: Walk) {
        // cx.begin_turtle(walk, Layout::default());
        cx.begin_turtle(walk, self.layout);
        let mut arr: Vec<Vec<String>> = Vec::new();
        for inner_vec in &self.select_choices_values {
            // Append the inner vector to the new variable
            arr.push(inner_vec.clone());
        }

        for (inneridx, choices_arr) in arr[0].iter().enumerate() {
            // println!("=>{:?}", choices_arr);
            // for (inneridx, _innerj) in choices_arr.iter().enumerate() {
            // self.draw_page_with_offset(
            //     cx,
            //     scope,
            //     inneridx as u8,
            //     self.choice_page_offset - SELECT_MAX_OFFSET,
            //     walk,
            // );
            let offset: f64 = self.choice_page_offset - SELECT_MAX_OFFSET;
            let index: u8 = inneridx as u8;
            // println!("=>{:?}", index);
            let widget_id = LiveId::from_str(&format!("page{}", index));
            let page = self.pages.get_or_insert(cx, widget_id, |cx| {
                WidgetRef::new_from_ptr(cx, self.selects_template)
            });
            // let _ = page.draw_walk_all(cx, scope, walk.with_margin_left(offset));
            //.draw_all(cx, scope);

            let _ = page.draw_walk(cx, scope, walk.with_margin_left(offset));
            // }
        }
        cx.end_turtle_with_area(&mut self.area);
    }

    fn draw_page_with_offset(
        &mut self,
        cx: &mut Cx2d,
        scope: &mut Scope,
        index: u8,
        offset: f64,
        walk: Walk,
    ) {
        let widget_id = LiveId::from_str(&format!("page{}", index));
        let page = self.pages.get_or_insert(cx, widget_id, |cx| {
            WidgetRef::new_from_ptr(cx, self.selects_template)
        });

        // let _ = page.draw_all(cx, scope);
        let _ = page.draw_walk(cx, scope, walk.with_margin_left(offset));
    }

    fn update_current_page(&mut self) {
        self.current_page = (self.current_page + 1) % self.select_choices_values[0].len() as u8;
        log!(
            "SelectOptions::update_current_page: {:?}",
            &self.current_page
        );
    }
}
