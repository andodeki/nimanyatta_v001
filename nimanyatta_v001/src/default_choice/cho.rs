// use makepad_widgets::*;
use crate::makepad_widgets::*;
const CHOICE_MAX_OFFSET: f64 = 700.0;
const SELECT_MAX_OFFSET: f64 = 500.0;
// use crate::shared::custom_button::CustomButton;

live_design! {
    import makepad_draw::shader::std::*;
    import makepad_widgets::base::*;
    import makepad_widgets::theme_desktop_dark::*;
    import crate::shared::styles::*;
    import crate::shared::steps::StepNav;
    import crate::shared::custom_button::CustomButton;

    Indicator = <RoundedView> {
        width: 100.0
        height: 8.0
        draw_bg: {
            color: #f60,
            radius: 2.5
        }
    }
    ButChoices = <View>{
        flow: Down,
        show_bg: false
        draw_bg: {
            color: #000
        }
        align: {x: 0.5, y: 0.5}
        but = <CustomButton> {
            width: 300, height: 45,
            text: "Option 1"
            draw_bg: {
                border_radius: 2.
            }
        }
    }
    SelectOptions = {{SelectOptions}} {
        // page_titles: []
        width: Fit, height: Fit,
        // indicator_titles: []
        flow: Down,
        spacing: 10,
        align: {x: 0.5, y: 0.5},
        select_choices_values: []
        selects_template: <ButChoices> {
            flow: Down,
        }
        choice_page_offset: 0.0
        animator: {
            selectopt = {
                default: restart,
                restart = {
                    from: {all: Snap}
                    apply: {choice_page_offset: 500.0}
                }
                show = {
                    redraw: false,
                    from: {all: Forward {duration: 0}}
                    apply: {choice_page_offset: 0.0}
                }
            }
        }

    }
    OptionC = {{Indicators}} {
        width: Fit,
        height: Fit,
        flow: Down,
        spacing: 5,
        align: {x: 0.5, y: 0.5},
        indicator_titles: []
        template: <CustomButton> {
            width: 100, height: Fit,
            // margin: 0, padding: 0
            text: "Option 1"
            draw_bg: {
                border_radius: 2.
            }
        }
    }
    StepsScreen = <View> {
        // width: 300,
        height: Fit,
        flow: Down,
        padding: 10.0
        align: {x: 0.5, y: 0.5}
        padding: 20,
        spacing: 20,
        step_title = <Label> {
            // margin: {top: 1}
            draw_text: {
                text_style: <H2_TEXT_BOLD> {},
                color: (COLOR_DOWN_6)
            }
            text: "Step One"
        }
        // <ButChoices> {}
        <SelectOptions> {
            // select_choices_values: [
            //     ["STP1-Option 1","STP1-Option 2","STP1-Option 3"],
            //     ["STP2-Option 1", "STP2-Option 2", "STP2-Option 3", "STP2-Option 4"],
            //     ["STP3-Option 1", "STP3-Option 2", "STP3-Option 3", "STP3-Option 4", "STP3-Option 5"]
            // ]
            select_choices_values: []
        }
        next = <CustomButton> {
            width: 300, height: 50,
            // margin: 0, padding: 0
            text: "Next"
            draw_bg: {
                border_radius: 2.
                fn pixel(self) -> vec4 {
                    let border_color = #0157c0;
                    // let border_color = #016def; //#0157c0
                    let border_width = 0.5;
                    let sdf = Sdf2d::viewport(self.pos * self.rect_size);
                    let body = mix(mix(#0157c0, #f, self.hover), #0157c0, self.pressed);

                    sdf.box(
                        1.,
                        1.,
                        self.rect_size.x - 2.0,
                        self.rect_size.y - 2.0,
                        self.border_radius
                    )
                    sdf.fill_keep(body)

                    sdf.stroke(
                        border_color,
                        border_width
                    )
                    return sdf.result
                }
            }
        }

    }
    IndicatorCheckBox = <CheckBox> {
        width: Fill,
        height: 35,
        margin: {left: 1},
        label_walk: {margin: {top: 15}}
        draw_check: {
            uniform size: 3.5;
            instance open: 0.0
            uniform length: 3.0
            uniform width: 1.0

            fn pixel(self) -> vec4 {
                let sdf = Sdf2d::viewport(self.pos * self.rect_size)
                match self.check_type {
                    CheckType::Check => {
                        let sz = self.size;
                        let left = sz + 1.;
                        let up = sz + 7;
                        let c = self.rect_size * vec2(0.5, 0.5);
                        sdf.box(
                            left,
                            c.y - up,
                            25. * sz + 5,
                            1.8 * sz,
                            1.8
                        );

                        sdf.fill(#232323);
                        sdf.stroke(#000, 0.5 + 0.5 * self.dpi_dilate);

                        let isz = sz * 0.5;
                        let ileft = isz + 3;
                        let iup = sz + 7;
                        sdf.box(
                            ileft,
                            c.y - iup,
                            25. * sz + 5,
                            1.8 * sz,
                            1.8
                        );
                        sdf.fill(mix(#fff0, #016def, self.selected));
                    }
                }
                return sdf.result
            }
        }
        draw_text: {text_style: <THEME_FONT_LABEL> {}}
    }
    Indicators = {{Indicators}} {
        width: Fit,
        height: Fit,
        flow: Right,
        // flow: Down,
        spacing: 5,
        // align: {x: 0.5, y: 0.5},
        indicator_titles: []
        template: <IndicatorCheckBox> {
            width: 100, height: 20,
            margin: 0, padding: 0
            // flow: Right,
            // flow: Down,
            align: {x: 0.5, y: 0.5},
            text: "Here",
            draw_text: {
                    fn get_color(self) -> vec4 {
                        return #f60;
                    }
                }
            draw_check: {
                check_type: Check,
            }
        }
    }
    Steps = {{Steps}} {
        page_titles: []
        choices_values: []
        page_template: <StepsScreen> { }
        // custom_button: <CustomButton>{
        //     width: Fill, height: Fit,
        //     text: "Next"
        //     draw_bg: {
        //         border_radius: 2.
        //     }
        // }
        choice_page_offset: 0.0
        animator: {
            choice = {
                default: restart,
                restart = {
                    from: {all: Snap}
                    apply: {choice_page_offset: 400.0}
                }
                show = {
                    redraw: true,
                    from: {all: Forward {duration: 0.5}}
                    apply: {choice_page_offset: 0.0}
                }
            }
        }
    }
    Choose = {{Choose}}{
        // debug: A
        width: 400,
        height: 480,
        flow: Down,
        padding: 20, spacing: 10
        align: {x: 0.5, y: 0.5}
        <Indicators>{
            indicator_titles: [
                "Step One",
                "Step Two",
                "Step Three"
            ]
        }
        <Steps>{
            page_template: <StepsScreen> {}
            page_titles: ["Step One", "Step Two", "Step Three" ]
            choices_values: [
                ["STP1-Option 1"],
                ["STP2-Option 1", "STP2-Option 2", "STP2-Option 3"],
                ["STP3-Option 1", "STP3-Option 2"]
            ]
        }
    }

}
#[derive(Live, LiveHook, Widget)]
pub struct Choose {
    #[deref]
    view: View,
}
impl Widget for Choose {
    fn draw_walk(&mut self, cx: &mut Cx2d, scope: &mut Scope, walk: Walk) -> DrawStep {
        self.view.draw_walk_all(cx, scope, walk);
        // log!("Choose: draw_walk");

        DrawStep::done()
    }

    fn handle_event(&mut self, cx: &mut Cx, event: &Event, scope: &mut Scope) {
        self.view.handle_event(cx, event, scope);
    }
}
#[derive(Live, Widget)]
pub struct Indicators {
    #[redraw]
    #[rust]
    area: Area,

    #[walk]
    walk: Walk,

    #[layout]
    layout: Layout,

    #[live]
    indicator_titles: Vec<String>,

    #[live]
    template: Option<LivePtr>,

    #[rust]
    items: ComponentMap<LiveId, WidgetRef>,
}

impl LiveHook for Indicators {
    fn after_apply(&mut self, cx: &mut Cx, _apply: &mut Apply, _index: usize, _nodes: &[LiveNode]) {
        // let tags = ["test1", "test2", "test3"];
        // log!("Indicators: after_apply");

        self.items.clear();
        for (i, title_text) in self.indicator_titles.iter().enumerate() {
            // for (i, title_text) in tags.iter().enumerate() {
            let item_id = LiveId::from_str(&format!("items{}", i));
            let item_widget = WidgetRef::new_from_ptr(cx, self.template);
            // item_widget.apply_over(cx, live! {label = { text: (title_text) }});
            item_widget.apply_over(cx, live! {text: (title_text) });
            self.items.insert(item_id, item_widget);
        }
    }
}

impl Widget for Indicators {
    fn handle_event(&mut self, cx: &mut Cx, event: &Event, scope: &mut Scope) {
        for (_id, item) in self.items.iter_mut() {
            item.handle_event(cx, event, scope);
        }
    }

    fn draw_walk(&mut self, cx: &mut Cx2d, scope: &mut Scope, walk: Walk) -> DrawStep {
        cx.begin_turtle(walk, self.layout);
        //Indicators
        // log!("Indicators: draw_walk");
        for (_id, item) in self.items.iter_mut() {
            // let _ = item.draw(cx, scope);
            let _ = item.draw_all(cx, scope);
        }

        cx.end_turtle_with_area(&mut self.area);
        DrawStep::done()
    }
}
#[derive(Live, Widget)]
pub struct Steps {
    #[redraw]
    #[rust]
    area: Area,

    #[walk]
    walk: Walk,

    // #[live]
    // draw_bg: DrawQuad,
    // #[live]
    // custom_button: CustomButton,
    #[live]
    page_titles: Vec<String>,
    #[live]
    choices_values: Vec<Vec<String>>,

    #[rust]
    items: ComponentMap<LiveId, WidgetRef>,
    // #[live]
    // choices_values: Vec<LiveValue>,
    #[live]
    page_template: Option<LivePtr>,

    #[live]
    options_template: Option<LivePtr>,

    #[rust(0)]
    current_page: u8,

    #[rust]
    pages: ComponentMap<LiveId, WidgetRef>,

    #[live]
    choice_page_offset: f64,

    #[animator]
    animator: Animator,
}

impl LiveHook for Steps {
    fn after_new_from_doc(&mut self, cx: &mut Cx) {
        log!("Steps::after_new_from_doc");
        // fn after_apply(&mut self, cx: &mut Cx, _apply: &mut Apply, _index: usize, _nodes: &[LiveNode]) {
        // log!("Steps: after_apply");
        for (idx, title_text) in self.page_titles.iter().enumerate() {
            let widget_id = LiveId::from_str(&format!("page{}", idx));
            let page = self.pages.get_or_insert(cx, widget_id, |cx| {
                WidgetRef::new_from_ptr(cx, self.page_template)
            });

            page.label(id!(step_title))
                .set_text_and_redraw(cx, &format!("{}", &title_text.as_str()))
        }
        // for (idx, title_text_vec) in self.choices_values.iter().enumerate() {
        //     log!("Options: {:?}", &title_text_vec);
        //     // for (_idx, title_text) in title_text_vec.iter().enumerate() {
        //     // log!("Options: {:?}", &title_text);
        //     let widget_id = LiveId::from_str(&format!("page{}", idx));
        //     let page = self.pages.get_or_insert(cx, widget_id, |cx| {
        //         WidgetRef::new_from_ptr(cx, self.page_template)
        //     });
        //     for (_idx, title_text) in title_text_vec.iter().enumerate() {
        //         log!("Options: {:?}", &title_text);
        //         page.button(id!(but))
        //             .set_text_and_redraw(cx, &format!("{}", &title_text.as_str()))
        //     }
        // }
    }
}

impl Widget for Steps {
    fn handle_event(&mut self, cx: &mut Cx, event: &Event, _scope: &mut Scope) {
        // //Options
        // for (_id, item) in self.items.iter_mut() {
        //     item.handle_event(cx, event, scope);
        // }
        // log!("Steps: handle_event");
        if self.animator_handle_event(cx, event).must_redraw() {
            self.redraw(cx);
        }

        // Fire the "show" animation when the "restart" animation is done
        if self.animator.animator_in_state(cx, id!(choice.restart)) {
            self.animator_play(cx, id!(choice.show));
        }

        // match event.hits(cx, self.custom_button.area()) {
        match event.hits(cx, self.area) {
            Hit::FingerUp(fe) => {
                if fe.is_over {
                    // Do not fire a new animation if the choice is already animating
                    if !self.animator.is_track_animating(cx, id!(choice)) {
                        self.update_current_page();
                        self.animator_play(cx, id!(choice.restart));
                        //self.redraw(cx);
                    }
                }
            }
            _ => (),
        };
    }
    // fn redraw(&mut self, cx: &mut Cx) {
    //     self.area.redraw(cx);
    // }

    fn draw_walk(&mut self, cx: &mut Cx2d, scope: &mut Scope, walk: Walk) -> DrawStep {
        // let walk = self.walk(cx);
        self.draw_walkd(cx, scope, walk);
        DrawStep::done()
    }
}

impl Steps {
    fn draw_walkd(&mut self, cx: &mut Cx2d, scope: &mut Scope, walk: Walk) {
        cx.begin_turtle(walk, Layout::default());

        self.draw_page_with_offset(cx, scope, self.current_page, self.choice_page_offset, walk);

        let prev_page_idx =
            (self.current_page + self.page_titles.len() as u8 - 1) % self.page_titles.len() as u8;
        self.draw_page_with_offset(
            cx,
            scope,
            prev_page_idx,
            self.choice_page_offset - CHOICE_MAX_OFFSET,
            walk,
        );

        cx.end_turtle_with_area(&mut self.area);
        // cx.end_turtle_with_area(&mut self.custom_button.area());
    }

    fn draw_page_with_offset(
        &mut self,
        cx: &mut Cx2d,
        scope: &mut Scope,
        index: u8,
        offset: f64,
        walk: Walk,
    ) {
        let widget_id = LiveId::from_str(&format!("page{}", index));
        let page = self.pages.get_or_insert(cx, widget_id, |cx| {
            WidgetRef::new_from_ptr(cx, self.page_template)
        });

        let _ = page.draw_walk(cx, scope, walk.with_margin_left(offset));
    }

    fn update_current_page(&mut self) {
        self.current_page = (self.current_page + 1) % self.page_titles.len() as u8;
        log!("Steps::update_current_page: {:?}", &self.current_page);
        // self.current_page = (self.current_page + 1) % self.choices_values.len() as u8;
    }
}

#[derive(Live, Widget)]
pub struct SelectOptions {
    #[redraw]
    #[rust]
    area: Area,

    #[walk]
    walk: Walk,

    #[layout]
    layout: Layout,

    #[live]
    page_titles: Vec<String>,

    #[live]
    select_choices_values: Vec<Vec<String>>,

    #[live]
    selects_template: Option<LivePtr>,

    #[live]
    options_template: Option<LivePtr>,

    #[rust(0)]
    current_page: u8,

    #[rust]
    pages: ComponentMap<LiveId, WidgetRef>,

    #[live]
    choice_page_offset: f64,

    #[animator]
    animator: Animator,
}
impl LiveHook for SelectOptions {
    fn after_new_from_doc(&mut self, cx: &mut Cx) {
        log!("SelectOptions::after_new_from_doc");
        // fn after_apply(&mut self, cx: &mut Cx, _apply: &mut Apply, _index: usize, _nodes: &[LiveNode]) {
        //     log!("SelectOptions: after_apply");
        self.select_choices_values = vec![
            vec![
                "STP1-Option 1".to_string(),
                "STP1-Option 2".to_string(),
                "STP1-Option 3".to_string(),
            ],
            vec![
                "STP2-Option 1".to_string(),
                "STP2-Option 2".to_string(),
                "STP2-Option 3".to_string(),
                "STP2-Option 4".to_string(),
            ],
            vec![
                "STP3-Option 1".to_string(),
                "STP3-Option 2".to_string(),
                "STP3-Option 3".to_string(),
                "STP3-Option 4".to_string(),
                "STP3-Option 5".to_string(),
            ],
        ];
        for (idx, title_text_vec) in self.select_choices_values.iter().enumerate() {
            // log!("Options: {:?}", &title_text_vec);
            let widget_id = LiveId::from_str(&format!("page{}", idx));
            let page = self.pages.get_or_insert(cx, widget_id, |cx| {
                WidgetRef::new_from_ptr(cx, self.selects_template)
            });
            for (_idx, title_text) in title_text_vec.iter().enumerate() {
                // log!("Options: {:?}", &title_text);
                page.button(id!(but))
                    .set_text_and_redraw(cx, &format!("{}", &title_text.as_str()))
            }
        }
    }
}

impl Widget for SelectOptions {
    fn handle_event(&mut self, cx: &mut Cx, event: &Event, _scope: &mut Scope) {
        if self.animator_handle_event(cx, event).must_redraw() {
            self.redraw(cx);
        }

        // Fire the "show" animation when the "restart" animation is done
        if self.animator.animator_in_state(cx, id!(selectopt.restart)) {
            self.animator_play(cx, id!(selectopt.show));
        }

        // match event.hits(cx, self.custom_button.area()) {
        match event.hits(cx, self.area) {
            Hit::FingerUp(fe) => {
                if fe.is_over {
                    // Do not fire a new animation if the selectopt is already animating
                    if !self.animator.is_track_animating(cx, id!(selectopt)) {
                        self.update_current_page();
                        self.animator_play(cx, id!(selectopt.restart));
                        // self.redraw(cx);
                    }
                }
            }
            _ => (),
        };
    }
    // fn redraw(&mut self, cx: &mut Cx) {
    //     self.area.redraw(cx);
    // }

    fn draw_walk(&mut self, cx: &mut Cx2d, scope: &mut Scope, walk: Walk) -> DrawStep {
        // let walk = self.walk(cx);
        self.draw_walkd(cx, scope, walk);
        DrawStep::done()
    }
}

impl SelectOptions {
    fn draw_walkd(&mut self, cx: &mut Cx2d, scope: &mut Scope, walk: Walk) {
        // cx.begin_turtle(walk, Layout::default());
        cx.begin_turtle(walk, self.layout);

        let mut arr: Vec<Vec<String>> = Vec::new();
        for inner_vec in &self.select_choices_values {
            // Append the inner vector to the new variable
            arr.push(inner_vec.clone());
        }
        // let arr = self.add_to();

        for (_idx, choices_arr) in arr.iter().enumerate() {
            for (inneridx, _innerj) in choices_arr.iter().enumerate() {
                // let prev_page_idx = (idx as u8 + choices_arr.len() as u8 - inneridx as u8)
                //     % choices_arr.len() as u8;
                // log!(
                //     "prev_page_idx/index: {:?}===offset: {:?}",
                //     inneridx,
                //     self.choice_page_offset - SELECT_MAX_OFFSET
                // );
                // if prev_page_idx == 1 {
                self.draw_page_with_offset(
                    cx,
                    scope,
                    inneridx as u8,
                    self.choice_page_offset - SELECT_MAX_OFFSET,
                    walk,
                );
                // }
            }
        }
        // self.draw_page_with_offset(cx, scope, self.current_page, self.choice_page_offset, walk);

        // let prev_page_idx = (self.current_page + self.select_choices_values.len() as u8 - 0)
        //     % self.select_choices_values.len() as u8;
        // self.draw_page_with_offset(
        //     cx,
        //     scope,
        //     prev_page_idx,
        //     self.choice_page_offset - SELECT_MAX_OFFSET,
        //     walk,
        // );

        cx.end_turtle_with_area(&mut self.area);
    }

    fn draw_page_with_offset(
        &mut self,
        cx: &mut Cx2d,
        scope: &mut Scope,
        index: u8,
        offset: f64,
        walk: Walk,
    ) {
        let widget_id = LiveId::from_str(&format!("page{}", index));
        let page = self.pages.get_or_insert(cx, widget_id, |cx| {
            WidgetRef::new_from_ptr(cx, self.selects_template)
        });

        // let _ = page.draw_all(cx, scope);
        let _ = page.draw_walk(cx, scope, walk.with_margin_left(offset));
    }

    fn update_current_page(&mut self) {
        self.current_page = (self.current_page + 1) % self.select_choices_values.len() as u8;
        log!(
            "SelectOptions::update_current_page: {:?}",
            &self.current_page
        );
    }
}
