// use crate::shared::stack_view_action::StackViewAction;
use makepad_widgets::*;
// use std::collections::HashMap;

live_design! {

    import makepad_draw::shader::std::*;
    import makepad_widgets::base::*;
    import makepad_widgets::theme_desktop_dark::*;

    import crate::home::home_screen::HomeScreen;
    import crate::default_choice::default_choice_screen::DefaultChoice;
    // import crate::default_choice::choose::Choose;
    import crate::two::two_screen::TwoScreen;
    import crate::three::three_screen::ThreeScreen;
    import crate::four::four_screen::FourScreen;
    import crate::five::five_screen::FiveScreen;
    import crate::shared::styles::*;
    import crate::shared::steps::Choices;
    import crate::shared::steps::ChoicesComponent;
    import crate::shared::popup_menu::MenuItem;
    // import crate::shared::header::DropDown;

    // import crate::shared::stack_navigation::*;ChoicesComponent


        REGULAR_TEXT = {
            font_size: (12),
            font: {path: dep("crate://makepad-widgets/resources/GoNotoKurrent-Regular.ttf")}
        }
        ICON_CHAT = dep("crate://self/resources/icons/chat.svg")
        PHONE_CHAT = dep("crate://self/resources/icons/chat.svg")

        AppTab2 = <RadioButton> {
            width: Fit,
            height: Fill,
            flow: Right,
            align: {x: 0.0, y: 0.0}
            margin: <SPACING_0> {}
            padding: <SPACING_0> {}
            // color: #000
            draw_radio: {
                radio_type: Tab,

                instance border_width: 0.0
                instance border_color: #0000
                instance inset: vec4(0.0, 0.0, 0.0, 0.0)
                instance radius: 2.5

                fn get_color(self) -> vec4 {
                    return mix(
                        mix(
                            (SIDEBAR_BG_COLOR),
                            (SIDEBAR_BG_COLOR_HOVER),
                            self.hover
                        ),
                        (SIDEBAR_BG_COLOR_SELECTED),
                        self.selected
                    )
                }

                fn get_border_color(self) -> vec4 {
                    return self.border_color
                }

                fn pixel(self) -> vec4 {
                    let sdf = Sdf2d::viewport(self.pos * self.rect_size)
                    sdf.box(
                        self.inset.x + self.border_width,
                        self.inset.y + self.border_width,
                        self.rect_size.x - (self.inset.x + self.inset.z + self.border_width * 2.0),
                        self.rect_size.y - (self.inset.y + self.inset.w + self.border_width * 2.0),
                        max(1.0, self.radius)
                    )
                    sdf.fill_keep(self.get_color())
                    if self.border_width > 0.0 {
                        sdf.stroke(self.get_border_color(), self.border_width)
                    }
                    return sdf.result;
                }
            }
            draw_text: {
                color_selected: #000, //#0b0,
                color_unselected: #000,
                color_unselected_hover: #111,
                text_style: <H3_TEXT_REGULAR> {}
            }
        }
        SIDEBAR_FONT_COLOR = #344054
        SIDEBAR_FONT_COLOR_HOVER = #344054
        SIDEBAR_FONT_COLOR_SELECTED = #127487

        // SIDEBAR_BG_COLOR = #F8F8F8
        SIDEBAR_BG_COLOR = #f
        SIDEBAR_BG_COLOR_HOVER = #E2F1F199
        SIDEBAR_BG_COLOR_SELECTED = #E2F1F199

        AppTab = <RadioButton> {
            width: 80,
            height: 70,
            padding: 0, margin: 0,
            flow: Down, spacing: 8.0, align: {x: 0.5, y: 0.5}

            icon_walk: {margin: 0, width: 30, height: 30}
            label_walk: {margin: 0}

            draw_radio: {
                radio_type: Tab,

                instance border_width: 0.0
                instance border_color: #0000
                instance inset: vec4(0.0, 0.0, 0.0, 0.0)
                instance radius: 2.5

                fn get_color(self) -> vec4 {
                    return mix(
                        mix(
                            (SIDEBAR_BG_COLOR),
                            (SIDEBAR_BG_COLOR_HOVER),
                            self.hover
                        ),
                        (SIDEBAR_BG_COLOR_SELECTED),
                        self.selected
                    )
                }

                fn get_border_color(self) -> vec4 {
                    return self.border_color
                }

                fn pixel(self) -> vec4 {
                    let sdf = Sdf2d::viewport(self.pos * self.rect_size)
                    sdf.box(
                        self.inset.x + self.border_width,
                        self.inset.y + self.border_width,
                        self.rect_size.x - (self.inset.x + self.inset.z + self.border_width * 2.0),
                        self.rect_size.y - (self.inset.y + self.inset.w + self.border_width * 2.0),
                        max(1.0, self.radius)
                    )
                    sdf.fill_keep(self.get_color())
                    if self.border_width > 0.0 {
                        sdf.stroke(self.get_border_color(), self.border_width)
                    }
                    return sdf.result;
                }
            }

            draw_text: {
                color_unselected: (SIDEBAR_FONT_COLOR)
                color_unselected_hover: (SIDEBAR_FONT_COLOR_HOVER)
                color_selected: (SIDEBAR_FONT_COLOR_SELECTED)

                fn get_color(self) -> vec4 {
                    return mix(
                        mix(
                            self.color_unselected,
                            self.color_unselected_hover,
                            self.hover
                        ),
                        self.color_selected,
                        self.selected
                    )
                }
            }

            draw_icon: {
                instance color_unselected: (SIDEBAR_FONT_COLOR)
                instance color_unselected_hover: (SIDEBAR_FONT_COLOR_HOVER)
                instance color_selected: (SIDEBAR_FONT_COLOR_SELECTED)
                fn get_color(self) -> vec4 {
                    return mix(
                        mix(
                            self.color_unselected,
                            self.color_unselected_hover,
                            self.hover
                        ),
                        self.color_selected,
                        self.selected
                    )
                }
            }
        }
        App = {{App}} {
        ui: <Window> {
            caption_bar = {
                margin: {
                    left: -100
                },
                visible: true,
                caption_label = {
                    label = {
                        text: "NiManyatta"
                    }
                }
            },
            window: {inner_size: vec2(1280, 1000)},

            body = {
                show_bg: true
                flow: Down,
                spacing: 20,
                align: {
                    x: 0.5,
                    y: 1.0
                },
                width: Fill,
                height: Fill

                draw_bg: {
                    fn pixel(self) -> vec4 {
                        return mix(#3, #1, self.pos.y);
                    }
                }
                navigation = <StackNavigation> {
                    root_view = {
                        width: Fill,
                        height: Fill,
                        padding: 0, align: {x: 1, y: 0.0}, spacing: 0., flow: Down
                        mobile_menu = <View> {
                            width: Fill,
                            height: 80,
                            // flow: Down,
                            show_bg: true
                            spacing: 0.0, padding: 0
                            draw_bg: {
                                instance radius: 0.0,
                                instance border_width: 0.0,
                                instance border_color: #000,
                                color: #fff
                            }

                            mobile_modes = <View> {
                                // spacing: 20
                                <View> {
                                    // padding: { right: 60 }
                                    home_tab = <AppTab> {
                                        flow: Right,
                                        align: {x: 0.0, y: 0.5}
                                        animator: {selected = {default: on}}
                                        text: ""
                                        draw_icon: {
                                            svg_file: (ICON_CHAT),
                                            fn get_color(self) -> vec4 {
                                                return mix(
                                                    #000,
                                                    #016def,//#0b0,
                                                    self.selected
                                                )
                                            }
                                        }
                                        width: 80.0,
                                        icon_walk: {width: 20, height: 20}
                                        flow: Right, spacing: 5.0, align: {x: 0.5, y: 0.5}
                                    }
                                }
                                <View> {
                                    align: {x: 0.5}
                                    two_tab = <AppTab> {
                                        flow: Right,
                                        align: {x: 0.0, y: 0.5}
                                        // animator: {selected = {default: on}}
                                        text: "2"
                                        draw_icon: {
                                            svg_file: (ICON_CHAT),
                                            fn get_color(self) -> vec4 {
                                                return mix(
                                                    #000,
                                                    #016def,//#0b0,
                                                    self.selected
                                                )
                                            }
                                        }
                                        width: 80.0,
                                        icon_walk: {width: 20, height: 20}
                                        flow: Down, spacing: 5.0, align: {x: 0.5, y: 0.5}
                                    }
                                }
                                <View> {
                                    align: {x: 0.5}
                                    three_tab = <AppTab> {
                                        flow: Right,
                                        align: {x: 0.0, y: 0.5}
                                        // animator: {selected = {default: on}}
                                        text: "3"
                                        draw_icon: {
                                            svg_file: (ICON_CHAT),
                                            fn get_color(self) -> vec4 {
                                                return mix(
                                                    #000,
                                                    #016def,//#0b0,
                                                    self.selected
                                                )
                                            }
                                        }
                                        width: 80.0,
                                        icon_walk: {width: 20, height: 20}
                                        flow: Down, spacing: 5.0, align: {x: 0.5, y: 0.5}
                                    }
                                }
                                <View> {
                                    align: {x: 0.5}
                                    four_tab = <AppTab> {
                                        flow: Right,
                                        align: {x: 0.0, y: 0.5}
                                        // animator: {selected = {default: on}}
                                        text: "4"
                                        draw_icon: {
                                            svg_file: (ICON_CHAT),
                                            fn get_color(self) -> vec4 {
                                                return mix(
                                                    #000,
                                                    #016def,//#0b0,
                                                    self.selected
                                                )
                                            }
                                        }
                                        width: 80.0,
                                        icon_walk: {width: 20, height: 20}
                                        flow: Down, spacing: 5.0, align: {x: 0.5, y: 0.5}
                                    }
                                }
                                <View> {
                                    align: {x: 1.0}
                                    five_tab = <AppTab> {
                                        flow: Right,
                                        align: {x: 0.0, y: 0.5}
                                        // animator: {selected = {default: on}}
                                        text: ""
                                        draw_icon: {
                                            svg_file: (PHONE_CHAT),
                                            fn get_color(self) -> vec4 {
                                                return mix(
                                                    #000,
                                                    #016def,//#0b0,
                                                    self.selected
                                                )
                                            }
                                        }
                                        width: 80.0,
                                        icon_walk: {width: 20, height: 20}
                                        flow: Right, spacing: 5.0, align: {x: 0.5, y: 0.5}
                                    }
                                }
                            }

                        }
                        application_pages = <View> {
                            margin: 0.0,
                            padding: 0.0

                            home_frame = <HomeScreen> { visible: true }
                            two_frame = <TwoScreen> {visible: false}
                            three_frame = <ThreeScreen> {visible: false}
                            four_frame = <FourScreen> {visible: false}
                            five_frame = <FiveScreen> {visible: false}
                        }
                    }

                    default_choices_stack_view = <StackNavigationView> { //default_choices_stack_view
                        header = {
                            content = {
                                title_container = {
                                    title = {
                                        text: "Help Me Choose"
                                        draw_text: {
                                            color: #000
                                        }
                                    }
                                }
                            }
                        }
                        body = {
                            <DefaultChoice> {}
                        }
                    }
                }
            }
        }
    }
}

app_main!(App);

#[derive(Live)]
pub struct App {
    #[live]
    ui: WidgetRef,
    // #[rust]
    // navigation_destinations: HashMap<StackViewAction, LiveId>,
}

impl LiveRegister for App {
    fn live_register(cx: &mut Cx) {
        crate::makepad_widgets::live_design(cx);
        crate::home::home_screen::live_design(cx);
        crate::default_choice::default_choice_screen::live_design(cx);
        crate::default_choice::choice::live_design(cx);
        // crate::default_choice::choose::live_design(cx);
        crate::two::two_screen::live_design(cx);
        crate::three::three_screen::live_design(cx);
        crate::four::four_screen::live_design(cx);
        crate::five::five_screen::live_design(cx);

        crate::shared::styles::live_design(cx);
        crate::shared::custom_button::live_design(cx);
        crate::shared::round_slider::live_design(cx);
        crate::shared::steps::live_design(cx);
        crate::shared::cho::live_design(cx);
        crate::shared::popup_menu::live_design(cx);
        crate::shared::header::live_design(cx);
        crate::shared::helpers::live_design(cx);

        crate::shared::dropdown_menu::live_design(cx);
    }
}

impl LiveHook for App {
    // fn after_new_from_doc(&mut self, _cx: &mut Cx) {
    //     self.init_navigation_destinations();
    // }
    fn after_new_from_doc(&mut self, _cx: &mut Cx) {
        println!("after_new_from_doc(): starting some kind of a loop");
        // crate::sliding_sync::start_matrix_tokio().unwrap();
    }
}
impl MatchEvent for App {
    fn handle_startup(&mut self, _cx: &mut Cx) {
        log!("App::handle_startup(): starting nigig sdk loop");
        // crate::sliding_sync::start_matrix_tokio().unwrap();
    }
    fn handle_shutdown(&mut self, _cx: &mut Cx) {
        log!("App::handle_shutdown()");
    }
    fn handle_pause(&mut self, _cx: &mut Cx) {
        log!("App::handle_pause()");
    }
    fn handle_resume(&mut self, _cx: &mut Cx) {
        log!("App::handle_resume()");
    }
    fn handle_app_got_focus(&mut self, _cx: &mut Cx) {
        log!("App::handle_app_got_focus()");
    }
    fn handle_app_lost_focus(&mut self, _cx: &mut Cx) {
        log!("App::handle_app_lost_focus()");
    }
    fn handle_actions(&mut self, cx: &mut Cx, actions: &Actions) {
        self.ui
            .radio_button_set(ids!(
                mobile_modes.home_tab,
                mobile_modes.two_tab,
                mobile_modes.three_tab,
                mobile_modes.four_tab,
                mobile_modes.five_tab,
            ))
            .selected_to_visible(
                cx,
                &self.ui,
                &actions,
                ids!(
                    application_pages.home_frame,
                    application_pages.two_frame,
                    application_pages.three_frame,
                    application_pages.four_frame,
                    application_pages.five_frame,
                ),
            );

        let mut navigation = self.ui.stack_navigation(id!(navigation));
        navigation.handle_stack_view_actions(cx, &actions);

        for action in actions {
            if let WindowAction::WindowGeomChange(ce) = action.as_widget_action().cast() {
                let screen_width = ce.new_geom.inner_size.x * ce.new_geom.dpi_factor;

                log!("x ===> {}", ce.new_geom.inner_size.x);
                log!("dpi_factor ===> {}", ce.new_geom.dpi_factor);
                log!("SCREEN_WIDTH ===> {}", screen_width);
            }
        }
    }
}

impl AppMain for App {
    fn handle_event(&mut self, cx: &mut Cx, event: &Event) {
        self.match_event(cx, event);
        self.ui.handle_event(cx, event, &mut Scope::empty());
    }
}

// impl Widget for App {
//     fn draw_walk(&mut self, cx: &mut Cx2d, scope: &mut Scope, walk: Walk) -> DrawStep {
//         // let label = self.view.label(id!(footer));
//         // let cap = self.ui.widget(id!(caption_bar));

//         // cap.apply_value_instance(cx, apply, index, nodes)

//         DrawStep::done()
//     }
// }

impl App {
    async fn _do_network_request(_cx: CxRef, _ui: WidgetRef, _url: &str) -> String {
        "".to_string()
    }
    // fn handle_event(&mut self, cx: &mut Cx, event: &Event) {
    //     if let Event::Draw(event) = event {
    //         return self.ui.draw_widget_all(&mut Cx2d::new(cx, event));
    //     }
    //     let actions = self.ui.handle_widget_event(cx, event);
    // }
}
