use makepad_widgets::widget::WidgetCache;
use makepad_widgets::*;

live_design! {
    import makepad_draw::shader::std::*;
    import makepad_widgets::base::*;
    import makepad_widgets::theme_desktop_dark::*;

    import crate::shared::styles::*;
    import crate::shared::custom_button::CustomButton;

    FourScreen = <View> {
        width: Fill, height: Fill
        flow: Down
        show_bg: true,
        draw_bg: {
            color: #fff
        }
        <View> {
            flow: Down,
            spacing: (SSPACING_2),

            align: {x: 0.5, y: 1}
            label = <Label> {
                margin: {top: 1}
                draw_text: {
                    text_style: <H2_TEXT_BOLD> {},
                    color: (COLOR_DOWN_6)
                }
                text: "Four"
            }
            // getstarted = <FishButton> {text: "Get Started"}
        }
    }
}

#[derive(Live, LiveHook, Widget)]
pub struct Four {
    #[deref]
    view: View,
    #[rust]
    counter: usize,
}

impl Widget for Four {
    fn handle_event(&mut self, cx: &mut Cx, event: &Event, scope: &mut Scope) {
        let actions = cx.capture_actions(|cx| self.view.handle_event(cx, event, scope));
        if self.view.button(id!(button1)).clicked(&actions) {
            log!("BUTTON CLICKED {}", self.counter);
            self.counter += 1;
            let label = self.view.label(id!(label1));
            label.set_text_and_redraw(cx, &format!("Counter: {}", self.counter));
            let user_prompt1 = self.view.text_input(id!(message_input.input)).text();
            log!("Click to count: {}", user_prompt1);
        }
    }

    fn draw_walk(&mut self, cx: &mut Cx2d, scope: &mut Scope, walk: Walk) -> DrawStep {
        self.view.draw_walk(cx, scope, walk)
    }
}
