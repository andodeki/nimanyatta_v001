// #![allow(unused)]
// #![cfg(target_arch = "wasm32")]
// use makepad_wasm_bridge::*;

#[cfg(not(target_arch = "wasm32"))]
use std::time::{SystemTime, UNIX_EPOCH};

// pub fn get_current_year() -> i32 {
//     let now = SystemTime::now();
//     let since_the_epoch = now.duration_since(UNIX_EPOCH).expect("Time went backwards");
//     let in_seconds = since_the_epoch.as_secs();
//     let seconds_per_year = 60 * 60 * 24 * 365;
//     let current_year = 1970 + (in_seconds / seconds_per_year as u64) as i32;
//     current_year
// }

#[cfg(not(target_arch = "wasm32"))]
pub fn get_current_year() -> i32 {
    let now = SystemTime::now();
    let since_the_epoch = now.duration_since(UNIX_EPOCH).expect("Time went backwards");
    let in_seconds = since_the_epoch.as_secs();
    let seconds_per_year = 60 * 60 * 24 * 365;
    let current_year = 1970 + (in_seconds / seconds_per_year as u64) as i32;
    current_year
}
