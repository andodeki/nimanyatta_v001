#[cfg(not(target_arch = "wasm32"))]
use crate::utils::get_current_year;

// #[cfg(target_arch = "wasm32")]
// use crate::utils::get_current_year;

use makepad_widgets::*;

live_design! {
    import makepad_widgets::base::*;
    import makepad_widgets::theme_desktop_dark::*;
    import makepad_draw::shader::std::*;
    import crate::shared::styles::*;

    import crate::shared::custom_button::CustomButton;



    FishPanel = <GradientYView> {
        flow: Down,
        padding: <SPACING_2> {}
        width: Fill,
        height: Fit
        draw_bg: {
            instance border_width: 0.0
            instance border_color: (COLOR_UP_FULL)
            instance inset: vec4(1.0, 1.0, 1.0, 1.0)
            instance radius: 2.5
            instance dither: 1.0
            color: (COLOR_UP_3),
            color2: (COLOR_UP_1)
            instance border_color: #x6
            instance border_color2: #x4
            instance border_color3: #x3A

            fn get_color(self) -> vec4 {
                let dither = Math::random_2d(self.pos.xy) * 0.04 * self.dither;
                return mix(self.color, self.color2, self.pos.y + dither)
            }

            fn pixel(self) -> vec4 {
                let sdf = Sdf2d::viewport(self.pos * self.rect_size)
                sdf.box(
                    self.inset.x + self.border_width,
                    self.inset.y + self.border_width,
                    self.rect_size.x - (self.inset.x + self.inset.z + self.border_width * 2.0),
                    self.rect_size.y - (self.inset.y + self.inset.w + self.border_width * 2.0),
                    max(1.0, self.radius)
                )
                sdf.fill_keep(self.get_color())
                if self.border_width > 0.0 {
                    sdf.stroke(
                        mix(
                            mix(self.border_color, self.border_color2, clamp(self.pos.y * 10, 0, 1)),
                            mix(self.border_color2, self.border_color3, self.pos.y),
                            self.pos.y
                        ),
                        self.border_width
                    )
                }
                return sdf.result;
            }
        }
    }

    FishPanelScrollY = <FishPanel> {
        height: Fill
        scroll_bars: <ScrollBars> {show_scroll_x: false, show_scroll_y: true}
    }

    Home = {{Home}} {
        flow: Down
        margin: <SPACING_0> {}
        padding: {top: (SSPACING_0)}

        <FishPanelScrollY> {
            width: Fill,
            height: Fill
            flow: Down,
            align: { y: 0.5}
            m = <View> {
                flow: Down,
                spacing: (SSPACING_2),

                align: {x: 0.5, y: 1}
                label = <Label> {
                    margin: {top: 1}
                    draw_text: {
                        text_style: <H2_TEXT_BOLD> {},
                        color: (COLOR_DOWN_6)
                    }
                    text: "Help Me Choose"
                }
                getstarted = <CustomButton> {
                    width: 250
                    height: Fit
                    text: "Get Started"
                    draw_bg: {
                        border_radius: 2.
                    }
                }
            }
            l = <View> {
                align: {x: 0.5, y: 1}
                // padding: {top: (SSPACING_0), bottom: (SSPACING_2)}

                footer = <Label> {
                    margin: {bottom: (SSPACING_1)}

                    draw_text: {
                        text_style: <H2_TEXT_BOLD> {},
                        color: #000
                        // color: (COLOR_UP_5)
                        // color: (COLOR_UP_6)
                    }
                    text: ""
                }
            }
        }
    }
    HomeScreen = <View> {
        width: Fill, height: Fill
        show_bg: true,
        draw_bg: {
            color: #fff
        }
        <Home> {}
    }
}

#[derive(Live, LiveHook, Widget)]
pub struct Home {
    #[deref]
    view: View,
}

impl Widget for Home {
    fn handle_event(&mut self, cx: &mut Cx, event: &Event, scope: &mut Scope) {
        let actions = cx.capture_actions(|cx| self.view.handle_event(cx, event, scope));
        if self.view.button(id!(getstarted)).clicked(&actions) {
            let uid = self.widget_uid();
            cx.widget_action(
                uid,
                &scope.path,
                StackNavigationAction::NavigateTo(live_id!(default_choices_stack_view)),
            );
            log!("GET STARTED BUTTON CLICKED");
        }
    }

    fn draw_walk(&mut self, cx: &mut Cx2d, scope: &mut Scope, walk: Walk) -> DrawStep {
        let label = self.view.label(id!(footer));
        // let check_box = self.view.check_box(id!(footer));
        // NiManyatta © 2024
        #[cfg(not(target_arch = "wasm32"))]
        let year = get_current_year();
        #[cfg(not(target_arch = "wasm32"))]
        label.set_text_and_redraw(cx, &format!("NiManyatta © {}", year));
        // check_box.set_text_and_redraw(cx, &format!("NiManyatta © {}", year));
        self.view.draw_walk(cx, scope, walk)
    }
}
